<section class="content">
    <div class="row">
        <div class="col-md-12">
        <div class='box'>
            <div class="box-header with-border">
                <div class="btn-group pull-right grid-create-btn" style="margin-right: 10px">
                    <a href="serial_project/create" class="btn btn-sm btn-success" title="序號產生">
                        <i class="fa fa-plus"></i><span class="hidden-xs">&nbsp;&nbsp;序號產生</span>
                    </a>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">用途</th>
                            <th scope="col">生產者</th>  
                            <th scope="col">數量</th>
                            <th scope="col">狀態</th>
                            <th scope="col">下載序號</th>
                            <th scope="col">預約序號建立時間</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach($data as $d)
                        <tr>
                            <th scope="row">{{ $d['id']}}</th>
                            <td>{{ $d['name']}}</td>
                            <td>{{ $d['creator']}}</td>
                            <td>{{ $d['amount']}}</td>
                            <td>
                            <?php 
                                //var_dump($d);
                                if($d['status']==1){
                                    echo "序號產生中";
                                }else{
                                    echo "序號已生成";
                                }
                            ?>
                            </td>
                            <td>
                            <?php 
                                if($d['status']==1){
                                    echo '<button type="button" class="btn btn-warning">等待</button>';
                                }else{
                                    echo "<a href='". url('excel')."/Glory_SN".$d['id']."_".$d['amount'].".csv' target='_blank' class='btn btn-info'>下載</button>";
                                }
                            ?>
                            </td>
                            <td>{{ $d['created_at'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
<section>