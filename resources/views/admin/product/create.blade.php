 <form action="http://local-admin.gloryflowshop.com/admin/country" method="post" accept-charset="UTF-8" class="form-horizontal" pjax-container="">

        <div class='box'>
           
            <div class="box-body">

                <div class="fields-group">

                    <div class="col-md-12">
                        <div class="form-group  ">
                            <label for="name" class="col-sm-2  control-label">產品名稱</label>

                            <div class="col-sm-8">
                                    <div class="input-group">

                                            <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                    
                                            <input type="text" id="name" name="name" value="" class="form-control name" placeholder="輸入 產品名稱">
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group  ">
                            <label for="name" class="col-sm-2  control-label">國家</label>

                            <div class="col-sm-8">
                                             <select class="form-control" style="width: 100%;" name="country_id" data-value="" >
                                                <?php foreach($countryData as $key=>$value){?>
                                                    <option value="{{$key}}">{{$value}}</option>
                                                <?php } ?>
                                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group  ">
                            <label for="name" class="col-sm-2  control-label">單次購買上限</label>

                            <div class="col-sm-8">
                                <input type="text" id="buy_limit" name="buy_limit" value="9" class="form-control buy_limit" placeholder="輸入 單次購買上限">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group  ">
                            <label for="name" class="col-sm-2  control-label">上／下架</label>

                            <div class="col-sm-8">
                                            <select class="form-control" style="width: 100%;" name="status" data-value="" >
    
                                                    <option value="1">上架</option>

                                                    <option value="2">下架</option>
                                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group  ">
                            <label for="name" class="col-sm-2  control-label">規格</label>

                            <div class="col-sm-8">
                                <input class="form-check-input" type="radio" name="type" id="type1" value="1" checked>
                                <label class="form-check-label" for="type1">
                                    無規格
                                </label>
                                <input class="form-check-input" type="radio" name="type" id="type2" value="2" checked>
                                <label class="form-check-label" for="type2">
                                    多種規格
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group  ">
                            <div class="pull-right">
                            <button type="submit" class="btn btn-primary">送出</button>
                            </div>
                        </div>
                    </div>
                </div>
 

            </div>
           
        </div>
</form>
