<b>親愛的 {{ $last_name }}{{ $first_name }}您好,</b>
<br/>
重置後密碼為： {{ $reset_password }}，10分鐘後此密碼將失效。
<br/>
若有任何問題，歡迎聯絡客服人員為您提供詳細解說，客服電話：(03) 558-6888
<br/>
Kind Regards,<br/>Glory