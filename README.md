##Start 
```
composer install
``` 
```
php artisan migrate
``` 
```
php artisan admin:install
```


FORMAT: 1.0
# Glory Flow Shop API

此API是用來提供給商品購物網站使用


## 1. Category 產品分類

### 1.1  取得商品分類

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/country/category?q=1

#### 動作 [GET]

* Parameters
    * q: `1` (required, interger) - 國家ID

* Response
    <pre>
    [
      {
         id: 1,
         text: "彩妝液"
      },
      {
         id: 2,
         text: "清潔液" 
      },
      ．．．．
    ]
   </pre>
   * id: 分類 ID
   * text: 分類名稱

### 1.2  取得分類內的商品

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/category/product_list?category_id=1&country_id=1

#### 動作 [GET]

* Parameters
    * country_id: `1` (required, interger) - 國家ID
    * category_id: `1` (required, interger) - 分類ID

* Response
   <pre>
    [
        {
            id: 2,
            name: "測試商品2",
            desc: "TEST Content",
            category_id: 1,
            country_id: 1,
            buy_limit: 9,
            status: 1,
            img1: "images/d84bf61acffcc2e38740481a9a365dff.jpeg",
            img2: null,
            img3: null,
            special_price_start_time: null,
            special_price_end_time: null,
            country_name: "台灣",
            category_name: "彩妝液",
            product_spec: [
                {
                    product_spec_id: 2,
                    is_special_price: 0,
                    spec_name: "單一規格",
                    weight: 1.8,
                    price: 1600,
                    special_price: 1500,
                    spec_img: "images/b2ae5c427cb9f654e6f1e6ebe944c22b.jpg"
                },
                ....
            ]
        },
        ....
    ]
   </pre>
   * id: Product ID
   * name: 商品名稱
   * desc: 商品描述
   * category_id: 分類ID
   * country_id: 國家ID
   * buy_limit: 購買上限
   * status: 上下架狀態 （ 1:上架 2:下架 ）
   * img1: 商品圖片1
   * img2: 商品圖片2
   * img3: 商品圖片3
   * special_price_start_time: 商品特價開始時間
   * special_price_end_time: 商品特價結束時間
   * country_name: 國家名稱,
   * category_name: 分類名稱,
   * product_spec: 產品規格
    -- product_spec_id: 商品規格ID
    -- is_special_price: 是否開啟特價 (1:是 0:否)
    -- spec_name: 商品規格名稱
    -- weight: 商品重量
    -- price: 商品售價
    -- special_price: 商品特價
    -- spec_img: 規格圖片

### 1.3 取得首頁可顯示的分類

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/category/index_show?country_id=1

#### 動作 [GET]

* Parameters
    * country_id: `1` (required, interger) - 國家ID

* Response
   <pre>
    [
        {
            id: 1,
            name: "彩妝液",
            product_amount: 10
        },
        {
            id: 2,
            name: "清潔液",
            product_amount: 88
        },
        {
            id: 3,
            name: "眉筆",
            product_amount: 99
        },
        {
            id: 4,
            name: "化妝用品",
            product_amount: 10
        },
        {
            id: 5,
            name: "香皂",
            product_amount: 88
        }
    ]
   </pre>
   * id: Category ID
   * name: 分類名稱
   * product_amount: 分類商品數
 

## 2.Banner 廣告

### 2.1  取得廣告列表

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/banner/list?country_id=1

#### 動作 [GET]

* Parameters
    * country_id: `1` (required, integer) - 國家ID

* Response
    <pre>
    [
        {
            id: 2,
            img_name: "images/7-cara-alami-miliki-kulit-putih.jpg",
            link: "eeeee"
        },
        {
            id: 3,
            img_name: "images/7-cara-alami-miliki-kulit-putih.jpg",
            link: "eeeee"
        },
        ....
    ]
   </pre>
   * id: Banner ID
   * img_name: 廣告圖片位置
   * link: 廣告圖片連結ＵＲＬ

## 3. Product 商品

### 3.1  取得一個商品資訊

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/product?country_id=1&product_id=3

#### 動作 [GET]

* Parameters
    * country_id: `1` (required, integer) - 國家ID
    * product_id: `3` (required, interger) - 商品ID

* Response
    <pre>
    [
        {
            id: 2,
            name: "測試商品2",
            desc: "TEST Content",
            keyword: "商品簡易描述",
            category_id: 1,
            country_id: 1,
            buy_limit: 9,
            status: 1,
            img1: "images/d84bf61acffcc2e38740481a9a365dff.jpeg",
            img2: null,
            img3: null,
            special_price_start_time: null,
            special_price_end_time: null,
            product_spec: [
                {
                    product_spec_id: 2,
                    spec_name: "單一規格",
                    weight: 1.8,
                    price: 1600,
                    is_special_price: 0,
                    special_price: 1500,
                    spec_img: "images/b2ae5c427cb9f654e6f1e6ebe944c22b.jpg"
                },
                ....
            ]
        },
        ....
    ]
   </pre>
   * id: Product ID
   * name: 商品名稱
   * desc: 商品描述
   * keyword: 商品簡易描述
   * category_id: 分類ID
   * country_id: 國家ID
   * buy_limit: 購買上限
   * status: 上下架狀態 （ 1:上架 2:下架 ）
   * img1: 商品圖片1
   * img2: 商品圖片2
   * img3: 商品圖片3
   * special_price_start_time: 商品特價開始時間
   * special_price_end_time: 商品特價結束時間
   * product_spec: 產品規格
    -- product_spec_id: 商品規格ID
    -- spec_name: 商品規格名稱
    -- weight: 商品重量
    -- price: 商品售價
    -- is_special_price: 是否開啟特價 ( 1: 是 0: 否 )
    -- special_price: 商品特價
    -- spec_img: 規格圖片

### 3.2  紀錄商品被瀏覽次數

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/product/click?country_id=1&product_id=3&product_spec_id=3&user_id=100001

#### 動作 [GET]

* Parameters
    * country_id: `1` (required, integer) - 國家ID
    * product_id: `3` (required, interger) - 商品 ID
    * product_spec_id: `3` (required, interger) - 商品規格 ID
    * user_id: `100001` (required, interger) - 使用者 ID

* Response 
   <pre>
    success
   </pre>
   or
   <pre>
    fail
   </pre>

### 3.3  商品搜尋

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/product/search?search_word=TEST&country_id=1

#### 動作 [GET]

* Parameters
    * search_word: `TEST` (required, string) - 商品名稱搜尋文字
    * country_id: `1` (required, integer) - 國家ID

* Response
    <pre>
    [
        {
            id: 5,
            name: "TEST PRODUCT",
            desc: "TEST PRODUCT CONTENT",
            keyword: "商品簡易描述"
            category_id: 2,
            country_id: 1,
            buy_limit: 10,
            status: 1,
            img1: "images/8196005c1de904885b88767a2f804a6a.jpg",
            img2: "images/BDL02_1000.jpg",
            img3: "images/Jean-D-Arcel-Technologie.jpg",
            special_price_start_time: null,
            special_price_end_time: null,
            country_name: "台灣",
            category_name: "清潔液",
            product_spec: [
                {
                    product_spec_id: 7,
                    spec_name: "規格0001",
                    weight: 1.8,
                    price: 29.99,
                    special_price: 19.99,
                    is_special_price: 0,
                    spec_img: "images/5560ef5cf7afc74237c70a9699d589e3.jpg"
                },
                ...
            ]
        },
        ...
    ]
   </pre>
   * id: Product ID
   * name: 商品名稱
   * desc: 商品描述
   * keyword: 商品簡易描述
   * category_id: 分類ID
   * country_id: 國家ID
   * buy_limit: 購買上限
   * status: 上下架狀態 （ 1:上架 2:下架 ）
   * img1: 商品圖片1
   * img2: 商品圖片2
   * img3: 商品圖片3
   * special_price_start_time: 商品特價開始時間
   * special_price_end_time: 商品特價結束時間
   * country_name: 國家名稱
   * category_name: 分類名稱
   * product_spec: 產品規格
    -- product_spec_id: 商品規格ID
    -- spec_name: 商品規格名稱
    -- weight: 商品重量
    -- price: 商品售價
    -- special_price: 商品特價
    -- is_special_price: 是否開啟特價 ( 1:是 0:否 )
    -- spec_img: 規格圖片

### 3.4  最新商品

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/product/new?country_id=1

#### 動作 [GET]

* Parameters
    * country_id: `1` (required, integer) - 國家ID

* Response
    <pre>
    [
        {
            id: 5,
            name: "TEST PRODUCT",
            desc: "TEST PRODUCT CONTENT",
            keyword: "商品簡易描述",
            category_id: 2,
            country_id: 1,
            buy_limit: 10,
            status: 1,
            img1: "images/8196005c1de904885b88767a2f804a6a.jpg",
            img2: "images/BDL02_1000.jpg",
            img3: "images/Jean-D-Arcel-Technologie.jpg",
            special_price_start_time: null,
            special_price_end_time: null,
            country_name: "台灣",
            category_name: "清潔液",
            product_spec: [
                {
                    is_special_price:1
                    product_spec_id: 7,
                    spec_name: "規格0001",
                    weight: 1.8,
                    price: 29.99,
                    special_price: 19.99,
                    spec_img: "images/5560ef5cf7afc74237c70a9699d589e3.jpg"
                },
                ...
            ]
        },
        ...
    ]
   </pre>
   * id: Product ID
   * name: 商品名稱
   * desc: 商品描述
   * keyword: 商品簡易描述
   * category_id: 分類ID
   * country_id: 國家ID
   * buy_limit: 購買上限
   * status: 上下架狀態 （ 1:上架 2:下架 ）
   * img1: 商品圖片1
   * img2: 商品圖片2
   * img3: 商品圖片3
   * special_price_start_time: 商品特價開始時間
   * special_price_end_time: 商品特價結束時間
   * country_name: 國家名稱
   * category_name: 分類名稱
   * product_spec: 產品規格
    -- is_special_price: 是否開啟特價 ( 1:是 0:否 )
    -- product_spec_id: 商品規格ID
    -- spec_name: 商品規格名稱
    -- weight: 商品重量
    -- price: 商品售價
    -- special_price: 商品特價
    -- spec_img: 規格圖片

## 4.Order 訂單

### 4.1  取得使用者訂單

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/order/list

#### 動作 [Post]
* headers: 
        
        Authorization: "Bearer " + access_token
* Parameters
    * None 無需參數

* Response
    <pre>
    [
        {
            order_id: "201909150203018689",
            user_id: 1000001,
            user_email: "carbo93@gmail.com",
            total_price: 2500,
            total_special_price: 2000,
            shipping_fee: 100,
            user_pay: 2100,
            country_id: 1,
            receiver_name: "林威志",
            phone: "+886921605435",
            zipcode: 106,
            address: "台北市大安區信義路四段265巷",
            user_payment_type: 1,
            user_payment_info: "4182-3087-****-****",
            order_shipping_status: 1,
            order_payment_status: 1,
            orderList: [
                {
                product_id: 1,
                product_name: "清潔液",
                product_spec_id: 1,
                spec_name: "單一規格",
                weight: 0.8,
                price: 2500,
                special_price: 2000,
                num: 1,
                total_price: 2500,
                total_special_price: 2000
                },
                ...
            ]
        },    
        ...
    ]
   </pre>
   * order_id: 訂單 ID
   * user_id: 使用者 ID
   * user_email: 使用者E-mail
   * total_price: 售價總額
   * total_special_price: 特價總額
   * shipping_fee: 運費
   * user_pay: 使用者實際付費
   * country_id: 國家ID
   * receiver_name: 收貨人名稱
   * phone: 收貨人電話
   * zipcode: 收貨人區碼
   * address: 收貨人地址
   * user_payment_type: 金流方式 ( 1:信用卡 )
   * user_payment_info: 付費資訊
   * order_shipping_status: 訂單狀態 (0:不限  1:訂單確認  ２:已出貨  3:配送中  4:已簽收)
   * order_payment_status: 交易狀態 (0:不限  1:已付款/授權成功  2:未處理  3:進入交易頁面  4:銀行交易中  5:使用者取消  6:交易失敗)
   * orderList: 訂單詳細列表
    -- product_id: 商品ID
    -- product_name: 商品名稱
    -- product_spec_id: 商品規格ID
    -- spec_name: 商品規格名稱
    -- weight: 商品重量
    -- price: 商品售價
    -- special_price: 商品特價
    -- num: 購買數量
    -- total_price: 商品總購買售價
    -- total_special_price: 商品總購買特價
    
### 4.2 建立訂單

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/order/create

#### 動作 [Post]
* headers: 
        
        Authorization: "Bearer " + access_token
* Parameters
<pre>
    {
       receiverData:{
            receiver_address: "台北市信義路四段",
            receiver_email: "carbo93@gmail.com",
            receiver_name: "林威志",
            receiver_note: "備註",
            receiver_phone: "電話",
            receiver_zipcode: "區碼",
       },
       userData:{
            email: "carbo93@gmail.com",
            first_name: "Wilson",
            last_name: "Lin",
            token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hZG1pbi5nbG9yeWZsb3dzaG9wLmNvbVwvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTkwMTY3NCwiZXhwIjoxNTcxOTA1Mjc0LCJuYmYiOjE1NzE5MDE2NzQsImp0aSI6IlFOWHk0QVl5VVFaT3lIRzciLCJzdWIiOjEwMDAwMSwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.kyf0CM3fCW5Ch5qxIjUP2aKSYuh4IDvVpdWu0aWWWjE",
            userID: 100001
        },
        paymentData:{
            finalDeliveryPrice: 100,
            finalProductPrice: 120,
            finalTotalPrice: 220
        },
        cartsData:[
            {
                buyNumber: 1,
                goodsDetail: {
                    buy_limit: 9,
                    category_id: 1,
                    country_id: 1,
                    name: "測試商品2",
                    status: 1,
                    desc: "TEST Content",
                    id: 2,
                    img1: "images/d84bf61acffcc2e38740481a9a365dff.jpeg",
                    img2: null,
                    img3: null,
                    special_price_start_time: null,
                    special_price_end_time: null,
                    product_spec: [
                        {
                            is_special_price: 1,
                            product_spec_id: 2,
                            spec_name: "單一規格",
                            weight: 1.8,
                            price: 1600,
                            special_price: 1500,
                            spec_img: "images/b2ae5c427cb9f654e6f1e6ebe944c22b.jpg"
                        },
                        ....
                    ]
                },
                productSpecID: 10,
                productSpecIndex: 0
            },
            ....
        ]
    }
</pre>

* Response
    <pre>
    {
        data:{
            msg: "交易成功"
            order_id: "2019102407213831565"
            status: "success"
        },
        status: 200
    }
   </pre>
 

## 5. Favorite 我的收藏

### 5.1  新增我的收藏

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/favorite/add

#### 動作 [POST]
* headers: 
        
        Authorization: "Bearer " + access_token
 
* Parameters
    * product_id: `3` (required, interger) - 商品ＩＤ
    * product_spec_id: `3` (required, interger) - 商品規格ＩＤ

* Response
   <pre>
    success
   </pre>
   or
   <pre>
    fail
   </pre>
   
### 5.2  刪除我的收藏

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/favorite/delete

#### 動作 [POST]
* headers: 
        
        Authorization: "Bearer " + access_token
 
* Parameters
    * user_id: `1000001` (required, interger) - 使用者ＩＤ
    * product_id: `3` (required, interger) - 商品ＩＤ
    * product_spec_id: `3` (required, interger) - 商品規格ＩＤ

* Response
   <pre>
    success
   </pre>
   or
   <pre>
    fail
   </pre>
   
### 5.3  取得我的收藏

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/favorite/list

#### 動作 [POST]
* headers: 
        
        Authorization: "Bearer " + access_token
 
* Parameters
    * None: 無需參數

* Response
<pre>
[
    [
        {
            "id": 4,
            "name": "商品四號",
            "desc": "<p>jjjjjj</p>",
            "keyword": null,
            "category_id": 9,
            "country_id": 3,
            "buy_limit": 9,
            "status": 1,
            "img1": "http://admin.gloryflowshop.com/uploads/images/baccc738a1dd3c23a14d2fe6c7422880.jpg",
            "img2": null,
            "img3": null,
            "special_price_start_time": null,
            "special_price_end_time": null,
            "product_spec": [
                {
                    "product_spec_id": 5,
                    "spec_name": "規格001",
                    "weight": 1.8,
                    "price": 40,
                    "special_price": 30,
                    "is_special_price": null,
                    "spec_img": "http://admin.gloryflowshop.com/uploads/images/5e59f9021f55d1afe807ebb2f3cf13dc.jpg"
                },
                ...
            ]
        }
    ]
]
</pre>
   * id: Product ID
   * name: 商品名稱
   * desc: 商品描述
   * keyword: 商品簡易描述
   * category_id: 分類ID
   * country_id: 國家ID
   * buy_limit: 購買上限
   * status: 上下架狀態 （ 1:上架 2:下架 ）
   * img1: 商品圖片1
   * img2: 商品圖片2
   * img3: 商品圖片3
   * special_price_start_time: 商品特價開始時間
   * special_price_end_time: 商品特價結束時間
   * product_spec: 產品規格
    -- product_spec_id: 商品規格ID
    -- spec_name: 商品規格名稱
    -- weight: 商品重量
    -- price: 商品售價
    -- special_price: 商品特價
    -- is_special_price: 是否開啟特價 ( 1:是 0:否 )    
    -- spec_img: 規格圖片
   
## 6. ShoppingCart 購物車

### 6.1  新增到購物車

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/shoppingCart/add?user_id=1000001&product_id=3&product_spec_id=3&num=1&price=100&special_price=99

#### 動作 [GET]
* headers: 
        
        Authorization: "Bearer " + access_token
 
* Parameters
    * product_id: `3` (required, interger) - 商品ID
    * product_spec_id: `3` (required, interger) - 商品規格ID
    * num: `1` (required, interger) - 購買數量
    * price: `100` (required, float) - 商品售價
    * special_price: `99` (required, float) - 商品特價

* Response
   <pre>
    success
   </pre>
   or
   <pre>
    fail
   </pre>

### 6.2  刪除購物車商品

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/shoppingCart/delete?shoppingCart_id=1

#### 動作 [GET]
* headers: 
        
        Authorization: "Bearer " + access_token
 
* Parameters
    * shoppingCart_id: `1` (required, interger) - 購物車ID

* Response
   <pre>
    success
   </pre>
   or
   <pre>
    fail
   </pre>
   

### 6.3  更新購物車商品

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/shoppingCart/update?shoppingCart_id=1&product_id=3&product_spec_id=3&num=1&price=100&special_price=99

#### 動作 [GET]
* headers: 
        
        Authorization: "Bearer " + access_token
 
* Parameters
    * shoppingCart_id: `1` (required, interger) - 購物車ＩＤ
    * product_id: `3` (required, interger) - 商品ＩＤ
    * product_spec_id: `3` (required, interger) - 商品規格ＩＤ
    * num: `1` (required, interger) - 購買數量
    * price: `100` (required, float) - 商品售價
    * special_price: `99` (required, float) - 商品特價
* Response
   <pre>
    success
   </pre>
   or
   <pre>
    fail
   </pre>


   
## 7. Login 使用者登入註冊

### 7.1  使用者註冊
#### 資源 [URI] 
http://admin.gloryflowshop.com/api/register

#### 動作 [POST]
* Parameters
    * email: `carbo93@gmail.com` (required, string) - 電子郵件(帳號)
    * password: `123456` (required, string) - 密碼
    * first_name: `Ｗilson` (required, string) -使用者名字
    * last_name: `Lin` (required, string) -使用者姓氏

* Response
   <pre>
    {
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbC1hZG1pbi5nbG9yeWZsb3dzaG9wLmNvbVwvYXBpXC9yZWdpc3RlciIsImlhdCI6MTU3MTg4ODkxMCwiZXhwIjoxNTcxODkyNTEwLCJuYmYiOjE1NzE4ODg5MTAsImp0aSI6IkwwWU9QUndFajBvMUVza3YiLCJzdWIiOjE0LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.OE7ZXll2bF9d94G-O169Rpj9lg6u-VLp0-CGjsKxidQ",
        "token_type": "bearer",
        "expires_in": 3600
    }
   </pre>
   * access_token: JWT認證用的Token
   * token_type: 認證類型
   * expires_in: Token時效時間(秒)

### 7.2  使用者登入
#### 資源 [URI] 
http://admin.gloryflowshop.com/api/login

#### 動作 [POST]
* Parameters
    * email: `carbo93@gmail.com` (required, string) - 電子郵件(帳號)
    * password: `123456` (required, string) - 密碼

* Response
   <pre>
    {
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hZG1pbi5nbG9yeWZsb3dzaG9wLmNvbVwvYXBpXC9sb2dpbiIsImlhdCI6MTU3MTg4OTY5MCwiZXhwIjoxNTcxODkzMjkwLCJuYmYiOjE1NzE4ODk2OTAsImp0aSI6ImxibzVpd1RnYzlYUjBscXYiLCJzdWIiOjEwMDAwMSwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.Z1H1NVzSpAp-1t1QzbwlmT-RFx0TGyQaPLl6QcWl17M",
        "token_type": "bearer",
        "expires_in": 3600
    }
   </pre>
   * access_token: JWT認證用的Token
   * token_type: 認證類型
   * expires_in: Token時效時間(秒)

### 7.3  使用者登出
#### 資源 [URI] 
http://admin.gloryflowshop.com/api/logout

#### 動作 [POST]
* headers: 
        
        Authorization: "Bearer " + access_token
* Parameters
    * none 無需參數

* Response
   <pre>
    {
      "message":"Successfully logged out"
    }
   </pre>
   * message: 返回訊息

### 7.4  使用者重設新密碼
#### 資源 [URI] 
http://admin.gloryflowshop.com/api/reset_password

#### 動作 [POST]
* Parameters
    * email: `carbo93@gmail.com` (required, string) - 電子郵件(帳號)
    * old_password: `123456` (required, string) - 舊密碼
    * new_password: `123456789` (required, string) - 新密碼
    * new_password2: `123456789` (required, string) - 再次輸入的新密碼

* Response
  
   <pre>
    reset_confirm_success
   </pre>
   or
   <pre>
    reset_confirm_fail
   </pre>

### 7.5  取得使用者資料

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/me

#### 動作 [GET]
* headers: 
        
        Authorization: "Bearer " + access_token
* Parameters
    * none 無需參數

* Response
   <pre>
    {
        "id": 100001,
        "first_name": "Wilson",
        "last_name": "Lin",
        "email": "carbo93@gmail.com",
        "created_at": "2019-10-07 03:49:53",
        "updated_at": "2019-10-24 02:32:56"
    }
   </pre>
   * id: 使用者ID
   * first_name: 使用者名字
   * last_name: 使用者姓氏
   * email: 電子郵件(帳號)
   * created_at: 創建時間
   * updated_at: 更新時間


## 8. 寄信相關
### 8.1 忘記密碼寄送重置密碼

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/email/forget_password

#### 動作 [POST]
* Parameters
   * email: `carbo93@gmail.com` (required, string) - 電子郵件(帳號)

* Response
   <pre>
    reset_password_success
   </pre>
   or
   <pre>
    reset_password_fail
   </pre>
   

## 9. 物流相關
### 9.1 物流列表

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/logistics/list

#### 動作 [GET]
* Parameters
   * None: `無需參數` 

* Response
   <pre>
   [
        {
            "id":1,
            "country_id":1,
            "name":"郵局宅配",
            "logistic_id":123456,
            "logistic_type":1
        },
        ...
   ]
   </pre>
   * id: 物流ID
   * country_id: 國家ID
   * name: 物流名稱
   * logistic_id: 物流廠商提供的ID(非必填)
   * logistic_type: 物流類型 1. 貨運   2.超商取貨  3.海外寄送

## 10. 金流相關
### 10.1 金流列表

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/payment/list

#### 動作 [GET]
* Parameters
   * None: `無需參數` 

* Response
   <pre>
    [
        {
            "id":1,
            "country_id":1,
            "name":"信用卡",
            "payment_id":"8752434"
            "payment_type":1
        },
        ...
    ]
   </pre>
   * id: 物流ID
   * country_id: 國家ID
   * name: 金流名稱
   * payment_id: 金流廠商的ID(非必填)
   * payment_type: 金流類型 1. 信用卡付款   2.超商付款

## 10. 國家相關
### 10.1 國家語言列表

#### 資源 [URI] 
http://admin.gloryflowshop.com/api/country/list

#### 動作 [GET]
* Parameters
   * None: `無需參數` 

* Response
   <pre>
    [
        {
            "id": 1,
            "name": "繁體中文"
        },
        {
            "id": 2,
            "name": "日本語"
        },
        {
            "id": 3,
            "name": "English"
        },
        {
            "id": 4,
            "name": "한국어"
        }
    ]
   </pre>
   * id: 國家語言ID
   * name: 國家語言名稱

