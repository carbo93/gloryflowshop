<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
class PasswordReset extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('emails.login.password_reset')
            /*
            ->attachRaw($this->pdf, 'name.pdf', [
                'mime' => 'application/pdf',
            ])
            */
            ->subject('會員密碼重置信件')
            ->with([
                'first_name' => $this->user->first_name,
                'last_name' => $this->user->last_name,
                'reset_password' => $this->user->reset_password
            ]);
    }
}
