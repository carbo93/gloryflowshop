<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GloryUserShippingInfo extends Model
{
    protected $table = 'glory_user_shipping_info';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'receiver_name',
        'country_id',
        'phone',
        'zip_code',
        'address',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = [];
}
