<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProductSpec;

class Product extends Model
{
    protected $table = 'product';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'creator',
        'name',
        'desc',
        'category_id',
        'country_id',
        'buy_limit',
        'status',
        'type',
        'img1',
        'img2',
        'img3',
        'special_price_start_time',
        'special_price_end_time',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = [];
    public function productspecs()
    {
        return $this->hasMany(ProductSpec::class);
    }
}
