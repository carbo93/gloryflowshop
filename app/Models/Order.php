<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\OrderList;

class Order extends Model
{
    protected $table = 'order';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'order_id',
        'user_id',
        'user_email',
        'total_price',
        'shipping_fee',
        'user_pay',
        'country_id',
        'receiver_name',
        'receiver_phone',
        'receiver_zipcode',
        'receiver_address',
        'receiver_email',
        'receiver_note',
        'user_payment_type',
        'user_payment_info',
        'user_shipping_type',
        'order_shipping_status',
        'order_payment_status'    
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = [];
    public function orderlists()
    {
        return $this->hasMany(OrderList::class);
    }
}
