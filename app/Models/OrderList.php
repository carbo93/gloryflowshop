<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderList extends Model
{

    protected $table = 'order_list';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'product_id',
        'product_img',
        'product_name',
        'product_spec_id',
        'spec_name',
        'weight',
        'is_special_price',
        'price',
        'special_price',
        'num',
        'total_price',
        'total_special_price'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = [];
    /*
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

}
