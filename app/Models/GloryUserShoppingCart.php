<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GloryUserShoppingCart extends Model
{
    protected $table = 'glory_user_shopping_cart';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'product_id',
        'product_spec_id',
        'num',
        'price',
        'special_price'   
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = [];
}
