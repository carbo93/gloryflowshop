<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GloryUserPaymentInfo extends Model
{
    protected $table = 'glory_user_payment_info';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'card_number',
        'expiry_date',
        'cvv',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = [];
}
