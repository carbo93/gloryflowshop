<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SerialNo extends Model
{
    protected $table = 'serial_no';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'serial_no',
        'creator'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = [];
}
