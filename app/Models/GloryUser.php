<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GloryUser extends Model
{
    protected $table = 'glory_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'country_id',
        'birth',
        'phone',
        'login_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = [];
}
