<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSpec extends Model
{

    protected $table = 'product_spec';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'spec_name',
        'weight',
        'original_price',
        'price',
        'special_price',
        'is_special_price',
        'amount',
        'spec_img'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = [];
    /*
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

}
