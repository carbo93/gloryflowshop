<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SerialProject extends Model
{
    protected $table = 'serial_project';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'creator',
        'amount'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = [];
}
