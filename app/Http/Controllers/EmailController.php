<?php

namespace App\Http\Controllers;

use App\User;
use App\Mail\PasswordReset;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use DB;
class EmailController extends Controller
{

    protected $user;

    public function __construct(
        User $user
    )
    {
        $this->user = $user;
    }


    public function forgetPassword(Request $request){

        
        $email = $request->get('email');
        
        $reset_password = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);

        $affectedRows = User::where('email', '=', $email)->update(
            [
                'reset_password' => $reset_password,
                'is_reset_password' => 1,
                'reset_password_at' => date("Y-m-d H:i:s")
            ]
        );

        $user = User::where('email', $email)->first();
        if($user){
            
            Mail::to($user->email)
            //->cc($moreUsers)
            //->bcc($evenMoreUsers)
            ->send(new PasswordReset($user));
    
            return 'reset_password_success';

        }else{

            return 'reset_password_fail';

        }
    }

}
