<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class PaymentController extends Controller
{

    /*
    API URL
    Method: Get
    http://admin.gloryflowshop.com/api/payment/list?country_id=1

    Return Array Json
    [
        {
        id: 2,
        img_name: "images/7-cara-alami-miliki-kulit-putih.jpg",
        link: "eeeee"
        }
    ]
    */
    public function list(Request $request){

        $country_id = $request->get('country_id');

        $now = date("Y-m-d H:i:s");

        $paymentLists = Payment::where('country_id', $country_id)
        ->where('status', 1)
        ->get(['id', 'country_id', 'name', 'payment_id']);

        return json_encode($paymentLists);
  
     }
    
}
