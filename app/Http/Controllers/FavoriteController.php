<?php

namespace App\Http\Controllers;

use App\Models\GloryUserFavorite;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ProductRepo;
use DB;
class FavoriteController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api']);
    }

    /*
    5.1 新增我的收藏
    API URL
    Method: POST
    http://local-admin.gloryflowshop.com/api/favorite/add
    Parameter
    user_id=1000001
    product_id=3
    product_spec_id=3

    Return String
    success
    or
    fail 
    */
    public function add(Request $request){

        //$user_id = $request->get('user_id');
        $user = auth()->user();
        $user_id = $user->id;

        $product_id = $request->get('product_id');

        $product_spec_id = $request->get('product_spec_id');

        try{
            $gloryUserFavorite = new GloryUserFavorite;

            $gloryUserFavorite->user_id = $user_id;

            $gloryUserFavorite->product_id = $product_id;
            
            $gloryUserFavorite->product_spec_id = $product_spec_id;

            $result = $gloryUserFavorite->save();
            
            if($result){
                return 'success';
            }else{
                return 'fail';
            }
        }catch(\Illuminate\Database\QueryException $e){
            return 'fail';
        }
     }


    /*
    5.2 刪除我的收藏
    API URL
    Method: POST
    http://local-admin.gloryflowshop.com/api/favorite/delete
    Parameter
    user_id=1000001
    favorite_id=1

    Return String
    success
    or
    fail 
    */
    public function delete(Request $request){

        //$user_id = $request->get('user_id');
        $user = auth()->user();
        $user_id = $user->id;

        $favorite_id = $request->get('favorite_id');

        $deletedRows = GloryUserFavorite::where('user_id', $user_id)
        ->where('id', $favorite_id)
        ->delete();
        
        if($deletedRows){
            return 'success';
        }else{
            return 'fail';
        }
    }

    /*
    5.3 取得我的收藏
    API URL
    Method: POST
    http://local-admin.gloryflowshop.com/api/favorite/list
    Parameter
    user_id=1000001

    Return String
    success
    or
    fail 
    */
    public function list(Request $request){

        $user = auth()->user();
        $user_id = $user->id;
        //$user_id = $request->get('user_id');

        $favorites = GloryUserFavorite::where('user_id', $user_id)
        ->get();
        $reuturn_data = [];
        foreach($favorites as $f){

           $product = ProductRepo::getProductDetailInfo($f->product_id);
           $reuturn_data[] = $product;
        }

        return json_encode($reuturn_data);
    }
    
}
