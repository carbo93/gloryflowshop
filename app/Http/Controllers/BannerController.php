<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class BannerController extends Controller
{

    /*
    API URL
    Method: Get
    http://admin.gloryflowshop.com/api/banner/list?country_id=1

    Return Array Json
    [
        {
        id: 2,
        img_name: "images/7-cara-alami-miliki-kulit-putih.jpg",
        link: "eeeee"
        }
    ]
    */
    public function list(Request $request){

        $country_id = $request->get('country_id');

        $now = date("Y-m-d H:i:s");

        $bannerLists = Banner::where('start_time', '<=', $now)
        ->where('end_time', '>=', $now)
        ->where('country_id', $country_id)
        ->get(['id', 'img_name', 'link']);

        $img_url = env('IMG_URL');


        foreach($bannerLists as $banner){
            $banner->img_name = $img_url.$banner->img_name;
        }

        return $bannerLists;
  
     }
    
}
