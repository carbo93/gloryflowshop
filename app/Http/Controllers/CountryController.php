<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class CountryController extends Controller
{
    
    /*
    API URL
    Method: Get
    http://local-admin.gloryflowshop.com/api/country/category?q=1

    Return Array Json
    [
      {
         id: 1,
         text: "彩妝液"
      },
      {
         id: 2,
         text: "清潔液" 
      },
    ]
    */
    public function category(Request $request){

      $country_id = $request->get('q');

      return Category::where('country_id', $country_id)->get(['id', DB::raw('name as text')]);


   }

   /*
    API URL
    Method: Get
    http://local-admin.gloryflowshop.com/api/country/list

    Return Array Json
    [
      {
         id: 1,
         name: "繁體中文"
      },
      {
         id: 2,
         text: "日本語" 
      },
      {
         id: 3,
         text: "English" 
      },
      {
         id: 4,
         text: "한국어" 
      },
    ]
    */
    public function list(Request $request){


      return Country::where('id', '>', '0')->get(['id', DB::raw('language_name as name')]);


   }
}
