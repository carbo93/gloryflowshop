<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Request;
class UsersController extends Controller
{


    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showProfile(Request $request){

        $flights = User::all();

        foreach ($flights as $flight) {
            echo $flight->name;
        }

    }
}
