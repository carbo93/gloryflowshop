<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\ProductSpec;
use App\Models\ProductClick;
use App\Repositories\CountryRepo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class ProductController extends Controller
{

    protected $countryRepo;

    public function __construct(
        CountryRepo $countryRepo
    )
    {
        $this->countryRepo = $countryRepo;
    }


    /*
    API URL
    取得單一個Product詳細資訊
    Method: Get
    http://local-admin.gloryflowshop.com/api/product?country_id=1&product_id=3

    Return Array Json
   [
        {
            id: 3,
            name: "商品3",
            desc: "<p>yyyyy</p>",
            category_id: 2,
            country_id: 2,
            buy_limit: 5,
            status: 2,
            img1: "images/4b8eb663b3ca49556451269420049269.jpg",
            img2: null,
            img3: null,
            special_price_start_time: null,
            special_price_end_time: null,
            product_spec: [
                {
                    product_spec_id: 3,
                    spec_name: "規格01",
                    weight: 1.9,
                    price: 13.15,
                    special_price: 12.15,
                    is_special_price: 1,
                    spec_img: "images/Makeup.jpg"
                },
                {
                    product_spec_id: 4,
                    spec_name: "規格02",
                    weight: 1.8,
                    price: 11.11,
                    special_price: 11.11,
                    is_special_price: 0,
                    spec_img: "images/473d76b55af12fdd5014bcb3779861e6.jpg"
                }
            ]
        }
    ]
    */
    public function getOneProductInfo(Request $request){

        
        $product_id = $request->get('product_id');

        $country_id = $request->get('country_id');

        $img_url = env('IMG_URL');

        $product = Product::where('id', $product_id)
        ->where('country_id', $country_id)
        ->get(
            [
                'id', 
                'name', 
                'desc', 
                'keyword',
                'category_id', 
                'country_id', 
                'buy_limit', 
                'status',
                'img1',
                'img2',
                'img3', 
                'special_price_start_time', 
                'special_price_end_time'
            ]
        );


        if(count($product)>0){

          $product[0]->product_spec = ProductSpec::where('product_id', $product_id)
          ->get([
              DB::raw('id as product_spec_id'),
              'spec_name',
              'weight',
              'price',
              'special_price', 
              'is_special_price',
              'spec_img'
            ]);

        }

        if($product[0]->img1!=''){
           $product[0]->img1 = $img_url.$product[0]->img1;
        }
        if($product[0]->img2!=''){
           $product[0]->img2 = $img_url.$product[0]->img2;
        }
        if($product[0]->img3!=''){
           $product[0]->img3 = $img_url.$product[0]->img3;
        }
        foreach($product[0]->product_spec as $spec){
           if($spec->spec_img!=''){
              $spec->spec_img = $img_url.$spec->spec_img;
           }
        }



        return $product;
   
    }


    /*
    API URL
    紀錄商品被瀏覽次數
    Method: Get
    http://local-admin.gloryflowshop.com/api/product/click?country_id=1&product_id=3&product_spec_id=3&user_id=100001

    Return String
    Success 
    or 
    Fail
    */

    public function product_click(Request $request){

        $country_id = $request->get('country_id');
        
        $product_id = $request->get('product_id');

        $product_spec_id = $request->get('product_spec_id');

        $user_id = $request->get('user_id');

        try{

            $productClick = new ProductClick;

            $productClick->country_id = $request->country_id;

            $productClick->product_id = $request->product_id;

            $productClick->product_spec_id = $request->product_spec_id;

            $productClick->user_id = $request->user_id;

            $result = $productClick->save();
        
            if($result){
            
                return 'success';
            
            }else{
            
                return 'fail';
            
            }

        }catch(\Illuminate\Database\QueryException $e){
            
            return 'fail';
        
        }

    }

    /*
    API URL
    商品搜尋
    Method: Get
    http://local-admin.gloryflowshop.com/api/product/search?search_word=TEST&country_id=1

    Return json

    */

    public function search(Request $request){

        $search_word = $request->get('search_word');
        $country_id = $request->get('country_id');
        $countryData = $this->countryRepo->getCountryArray();
        $categoryData = $this->countryRepo->getCategoryArray();
        $img_url = env('IMG_URL');
        try{

            $product = Product::where('name', 'like', '%'.$search_word.'%')
            ->where('country_id', $country_id)
            ->where('status', 1)
            ->get(['id', 'name', 'desc', 'keyword', 'category_id', 'country_id', 'buy_limit', 'status','img1','img2','img3', 'special_price_start_time', 'special_price_end_time']);
        
            foreach($product as $p){
                $country_id = $p->country_id;
                $category_id = $p->category_id;
                $p->country_name = $countryData[$country_id];
                $p->category_name = $categoryData[$category_id];
                $p->product_spec = ProductSpec::where('product_id', $p->id)->get([DB::raw('id as product_spec_id'),'spec_name', 'weight', 'price', 'special_price', 'is_special_price', 'spec_img']);
                if($p->img1!=''){
                   $p->img1 = $img_url.$p->img1;
                }
                if($p->img2!=''){
                   $p->img2 = $img_url.$p->img2;
                }
                if($p->img3!=''){
                   $p->img3 = $img_url.$p->img3;
                }
                foreach($p->product_spec as $spec){
                   if($spec->spec_img!=''){
                      $spec->spec_img = $img_url.$spec->spec_img;
                   }
                }
            }

            return $product;

        }catch(\Illuminate\Database\QueryException $e){
            return 'fail';
        }
    }



    /*
    API URL
    最新商品
    Method: Get
    http://local-admin.gloryflowshop.com/api/product/new?country_id=1

    Return json

    */

    public function new(Request $request){

        $country_id = $request->get('country_id');
        //dd($country_id);
        $countryData = $this->countryRepo->getCountryArray();
        $categoryData = $this->countryRepo->getCategoryArray();
        $img_url = env('IMG_URL');
        try{

            $product = Product::where('country_id', $country_id)
            ->where('status', 1)
            ->orderBy('id', 'DESC')
            ->take(10)
            ->get(['id', 'name', 'desc', 'keyword', 'category_id', 'country_id', 'buy_limit', 'status','img1','img2','img3', 'special_price_start_time', 'special_price_end_time']);
        
            foreach($product as $p){
                $country_id = $p->country_id;
                $category_id = $p->category_id;
                $p->country_name = $countryData[$country_id];
                $p->category_name = $categoryData[$category_id];
                $p->product_spec = ProductSpec::where('product_id', $p->id)->get([DB::raw('id as product_spec_id'),'spec_name', 'weight', 'price', 'special_price', 'is_special_price', 'spec_img']);
                if($p->img1!=''){
                   $p->img1 = $img_url.$p->img1;
                }
                if($p->img2!=''){
                   $p->img2 = $img_url.$p->img2;
                }
                if($p->img3!=''){
                   $p->img3 = $img_url.$p->img3;
                }
                foreach($p->product_spec as $spec){
                   if($spec->spec_img!=''){
                      $spec->spec_img = $img_url.$spec->spec_img;
                   }
                }
            }

            
            return $product;

        }catch(\Illuminate\Database\QueryException $e){
            return 'fail';
        }
    }
}
