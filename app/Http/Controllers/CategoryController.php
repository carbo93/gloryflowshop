<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductSpec;
use App\Models\Country;
use App\Models\Category;
use App\Repositories\CountryRepo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class CategoryController extends Controller
{
    protected $countryRepo;

    public function __construct(
       CountryRepo $countryRepo
    )
    {
       $this->countryRepo = $countryRepo;
    }
    /*
    API URL
    Method: Get
    http://local-admin.gloryflowshop.com/api/category/product_list?country_id=1&category_id=1

    Return Array Json

    */
    public function product_list(Request $request){

      $country_id = $request->get('country_id');

      $category_id = $request->get('category_id');
      
      $countryData = $this->countryRepo->getCountryArray();

      $categoryData = $this->countryRepo->getCategoryArray();

      try{

          $product = Product::where('category_id', $category_id)
          ->where('country_id', $country_id)
          ->where('status', 1)
          ->get(['id', 'name', 'desc', 'category_id', 'country_id', 'buy_limit', 'status', 'img1', 'img2', 'img3', 'special_price_start_time', 'special_price_end_time']);
          $img_url = env('IMG_URL');
          foreach($product as $p){
              $country_id = $p->country_id;
              $category_id = $p->category_id;
              $p->country_name = $countryData[$country_id];
              $p->category_name = $categoryData[$category_id];
              $p->product_spec = ProductSpec::where('product_id', $p->id)->get([DB::raw('id as product_spec_id'), 'is_special_price', 'spec_name', 'weight', 'price', 'special_price', 'spec_img']);
              if($p->img1!=''){
                 $p->img1 = $img_url.$p->img1;
              }
              if($p->img2!=''){
                 $p->img2 = $img_url.$p->img2;
              }
              if($p->img3!=''){
                 $p->img3 = $img_url.$p->img3;
              }
              foreach($p->product_spec as $spec){
                 if($spec->spec_img!=''){
                    $spec->spec_img = $img_url.$spec->spec_img;
                 }
              }

          }

          return $product;

      }catch(\Illuminate\Database\QueryException $e){
          return 'fail';
      }


   }

   /*
    API URL
    Method: Get
    http://local-admin.gloryflowshop.com/api/category/index_show?country_id=1

    Return Array Json

    */
    public function index_show(Request $request){

      $country_id = $request->get('country_id');
      
      try{

          $category = Category::where('country_id', $country_id)
          ->where('index_show', 1)
          ->take(5)
          ->get(['id', 'name', 'product_amount']);
          

          return $category;

      }catch(\Illuminate\Database\QueryException $e){
          return 'fail';
      }


   }
}
