<?php

namespace App\Http\Controllers;

use App\Models\GloryUserShoppingCart;
use App\Models\Order;
use App\Models\OrderList;
use App\Models\ProductSpec;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Log;
class OrderController extends Controller
{
    public function __construct()
    {
        //$this->middleware(['auth:api'])->except(['login','register']);
        $this->middleware(['auth:api']);
    }
    /* 4.1 取得使用者訂單  */
    public function getUserOrderList(Request $request){

        $img_url = env('IMG_URL');
        $user = auth()->user();
        $user_id = $user->id;

        $order = Order::where('user_id', $user_id)->orderBy('id', 'desc')->get(
            [
             'order_id', 
             'user_id', 
             'user_email', 
             'total_price', 
             'shipping_fee', 
             'user_pay',
             'country_id',
             'receiver_name',
             'receiver_phone', 
             'receiver_zipcode', 
             'receiver_address',
             'receiver_email',
             'user_payment_type',
             'user_payment_info',
             'user_shipping_type',
             'order_shipping_status',
             'order_payment_status',
             'created_at'
            ]
        );

        foreach($order as $o){

            $theOrderList = OrderList::where('order_id', $o->order_id)->get(
                [
                    'product_id',
                    'product_img',
                    'product_name',
                    'product_spec_id',
                    'spec_name', 
                    'weight', 
                    'is_special_price', 
                    'price', 
                    'special_price', 
                    'num', 
                    'total_price', 
                    'total_special_price'
                ]
            );

            foreach($theOrderList as $ol){
                if($ol->product_img!=''){
                   $ol->product_img = $img_url.$ol->product_img;
                }
            }
            
            $o->orderList = $theOrderList;
        }
        return json_encode($order);
     }
    
    /*  4.2 建立訂單  */
    public function createAnOrder(Request $request){
        
        $order_id = date("YmdHis").rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);

        $result = $request->all(); 
        $cartsData = $result['cartsData'];
        /*
        Log::info("START");
        Log::info($result);
        Log::info($result['receiverData']);
        $orderArray = [
            'order_id' => $order_id,
            'user_id' => $result['userData']['userID'],
            'user_email' => $result['userData']['email'],
            'total_price' => $result['paymentData']['finalProductPrice'],
            'shipping_fee' => $result['paymentData']['finalDeliveryPrice'],
            'user_pay' => $result['paymentData']['finalTotalPrice'],
            'country_id' => 1,
            'receiver_name' => $result['receiverData']['receiver_name'],
            'receiver_phone' => $result['receiverData']['receiver_phone'],
            'receiver_zipcode' => $result['receiverData']['receiver_zipcode'],
            'receiver_address' => $result['receiverData']['receiver_address'],
            'receiver_email' => $result['receiverData']['receiver_email'],
            'user_payment_type' => 1,
            'user_payment_info' => '',
            'user_shipping_type' => 1,
            'order_shipping_status' => 0,
            'order_payment_status' => 0
        ];

        Order::create($orderArray);
        
        $cartsArray = [];
        foreach($cartsData as $carts){
            $productSpecIndex = $carts['productSpecIndex'];
            $cartsArray['order_id'] = $order_id;
            $cartsArray['product_id'] = $carts['goodsDetail']['id'];
            $cartsArray['product_name'] = $carts['goodsDetail']['name'];
            $cartsArray['product_spec_id'] = $carts['goodsDetail']['product_spec'][$productSpecIndex]['product_spec_id'];
            $cartsArray['spec_name'] = $carts['goodsDetail']['product_spec'][$productSpecIndex]['spec_name'];
            $cartsArray['weight'] = $carts['goodsDetail']['product_spec'][$productSpecIndex]['weight'];
            $cartsArray['is_special_price'] = $carts['goodsDetail']['product_spec'][$productSpecIndex]['is_special_price'];
            $price = $carts['goodsDetail']['product_spec'][$productSpecIndex]['price'];
            $cartsArray['price'] = $price;
            $special_price = $carts['goodsDetail']['product_spec'][$productSpecIndex]['special_price'];
            $cartsArray['special_price'] = $special_price;
            $buyNumber = $carts['buyNumber'];
            $cartsArray['num'] = $buyNumber;
            $cartsArray['total_price'] = $price * $buyNumber;
            $cartsArray['total_special_price'] = $special_price * $buyNumber;


            OrderList::create($cartsArray);

            //需要再檢查資料庫的商品價錢和傳來的價錢是否一樣
            //
            //$pSpec = ProductSpec::where('id', productSpecID)->get();
            //$pSpec->
            
        }
      */

        
        /*
        Log::info('receiver_name => '.$result['receiverData']['receiver_name']);
        Log::info('finalProductPrice => '.$result['paymentData']['finalProductPrice']);
        Log::info('finalProductPrice => '.$result['paymentData']['finalProductPrice']);
        Log::info('email => '.$result['userData']['email']);
        Log::info('productSpecID => '.$result['cartsData'][0]['productSpecID']);
        */

        DB::beginTransaction();

        try {
            DB::table('order')->insert(
                [
                    'order_id' => $order_id,
                    'user_id' => $result['userData']['userID'],
                    'user_email' => $result['userData']['email'],
                    'total_price' => $result['paymentData']['finalProductPrice'],
                    'shipping_fee' => $result['paymentData']['finalDeliveryPrice'],
                    'user_pay' => $result['paymentData']['finalTotalPrice'],
                    'country_id' => 1,
                    'receiver_name' => $result['receiverData']['receiver_name'],
                    'receiver_phone' => $result['receiverData']['receiver_phone'],
                    'receiver_zipcode' => $result['receiverData']['receiver_zipcode'],
                    'receiver_address' => $result['receiverData']['receiver_address'],
                    'receiver_email' => $result['receiverData']['receiver_email'],
                    'receiver_note' => $result['receiverData']['receiver_note'],
                    'user_payment_type' => 1,
                    'user_payment_info' => '',
                    'user_shipping_type' => 1,
                    'order_shipping_status' => 0,
                    'order_payment_status' => 0,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]
            );
            
            $check_status = true;  //檢查價錢是否有被更動
            foreach($cartsData as $carts){
                $productSpecIndex = $carts['productSpecIndex'];
                $cartsArray = [];
                $cartsArray['order_id'] = $order_id;
                $cartsArray['product_id'] = $carts['goodsDetail']['id'];
                $cartsArray['product_name'] = $carts['goodsDetail']['name'];
                
                $product_spec_id = $carts['goodsDetail']['product_spec'][$productSpecIndex]['product_spec_id'];
                $cartsArray['product_spec_id'] = $product_spec_id;
                $cartsArray['spec_name'] = $carts['goodsDetail']['product_spec'][$productSpecIndex]['spec_name'];
                $cartsArray['weight'] = $carts['goodsDetail']['product_spec'][$productSpecIndex]['weight'];
                $is_special_price = $carts['goodsDetail']['product_spec'][$productSpecIndex]['is_special_price'];
                $cartsArray['is_special_price'] = $is_special_price ;
                $price = $carts['goodsDetail']['product_spec'][$productSpecIndex]['price'];
                $cartsArray['price'] = $price;
                $special_price = $carts['goodsDetail']['product_spec'][$productSpecIndex]['special_price'];
                $cartsArray['special_price'] = $special_price;
                $buyNumber = $carts['buyNumber'];
                $cartsArray['num'] = $buyNumber;
                $cartsArray['total_price'] = $price * $buyNumber;
                $cartsArray['total_special_price'] = $special_price * $buyNumber;
                $cartsArray['created_at'] = date("Y-m-d H:i:s");
                $cartsArray['updated_at'] = date("Y-m-d H:i:s");

    
                $pSpec = ProductSpec::where('id', $product_spec_id)->first();
                $cartsArray['product_img'] = $pSpec->spec_img;


                if($pSpec->price==$price&&$pSpec->special_price==$special_price&&$pSpec->is_special_price==$is_special_price){
                    DB::table('order_list')->insert(
                        $cartsArray
                    );
                }else{
                    $check_status = false;
                }
        
            }

            //檢查價錢是否有被更動
            if($check_status){
                DB::commit();
                return json_encode(
                    [
                    'status'=>'success',
                    'msg'=>'交易成功',
                    'order_id'=> $order_id 
                    ]
                );
            }else{
                DB::rollback();
                return json_encode(
                    [
                    'status'=>'fail',
                    'msg'=>'交易失敗!'
                    ]
                );
            }
           
            
        } catch (\Exception $e) {
            Log::info($e);
            DB::rollback();
            return json_encode(
                [
                'status'=>'fail',
                'msg'=>'交易失敗'
                ]
            );
        }
        
    }
}
