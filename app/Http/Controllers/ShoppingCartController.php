<?php

namespace App\Http\Controllers;

use App\Models\ShoppingCart;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class ShoppingCartController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api']);
    }
    /*
    API URL
    Method: Get
    http://local-admin.gloryflowshop.com/api/shoppingCart/add?
    user_id=1000001&product_id=3&product_spec_id=3&num=1&price=100&special_price=99

    Return String
    success
    or
    fail 
    */
    public function add(Request $request){

        //$user_id = $request->get('user_id');

        $user = auth()->user();
        $user_id = $user->id;

        $product_id = $request->get('product_id');

        $product_spec_id = $request->get('product_spec_id');

        $num = $request->get('num');

        $price = $request->get('price');

        $special_price = $request->get('special_price');

        try{

            $shoppingCart = new ShoppingCart;

            $shoppingCart->user_id = $user_id;

            $shoppingCart->product_id = $product_id;
            
            $shoppingCart->product_spec_id = $product_spec_id;

            $shoppingCart->num = $num;

            $shoppingCart->price = $price;

            $shoppingCart->special_price = $special_price;

            $result = $shoppingCart->save();
            
            if($result){
                return 'success';
            }else{
                return 'fail';
            }

        }catch(\Illuminate\Database\QueryException $e){
            return 'fail';
        }
     }


    /*
    API URL
    Method: Get
    http://local-admin.gloryflowshop.com/api/shoppingCart/delete?shoppingCart_id=1

    Return String
    success
    or
    fail 
    */
    public function delete(Request $request){

        $shoppingCart_id = $request->get('shoppingCart_id');

        $deletedRows = ShoppingCart::where('id', $shoppingCart_id)->delete();
        
        if($deletedRows){
            return 'success';
        }else{
            return 'fail';
        }

     }


     /*
    API URL
    Method: Get
    http://local-admin.gloryflowshop.com/api/shoppingCart/update?
    shoppingCart_id=1&user_id=1000001&product_id=3&product_spec_id=3&num=1&price=1000&special_price=990


    Return String
    success
    or
    fail 
    */
    public function update(Request $request){

        $shoppingCart_id = $request->get('shoppingCart_id');

        $user = auth()->user();
        $user_id = $user->id;
        //$user_id = $request->get('user_id');

        $product_id = $request->get('product_id');

        $product_spec_id = $request->get('product_spec_id');

        $num = $request->get('num');

        $price = $request->get('price');

        $special_price = $request->get('special_price');



        $affectedRows = ShoppingCart::where('id', $shoppingCart_id)
        ->update(
            [
                'user_id' => $user_id,
                'product_id' => $product_id,
                'product_spec_id' => $product_spec_id,
                'num' => $num,
                'price' => $price,
                'special_price' => $special_price
            ]
        );


        if($affectedRows){
            return 'success';
        }else{
            return 'fail';
        }

     }
    
}
