<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api'])->except(['login','register','reset_password']);
    }

    public function register(Request $request)
    {
        try{

            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email'    => $request->email,
                'password' => $request->password,
            ]);

            $token = auth()->login($user);

            return $this->respondWithToken($token);

        }catch(\Illuminate\Database\QueryException $e){

            return $this->respondWithToken('register_fail');

        }
    
    }

    public function reset_password(Request $request)
    {
        $email = $request->email;
        $old_password = $request->old_password;
        $new_password = $request->new_password;
        $new_password2 = $request->new_password2;

        if($new_password==$new_password2){

            $userResetObj = User::where('email', $email)->where('reset_password', $old_password)->first();
            
            //可以重設密碼
            
            if($userResetObj){
            
                $affectedRows = User::where('email', '=', $email)->update(
                    [
                        'reset_password' => '',
                        'is_reset_password' => 0,
                        'password' => Hash::make($new_password)
                    ]
                );

                return 'reset_confirm_success';

            }else{

                return 'reset_confirm_fail';
            
            }
        }else{

            return 'new_password_not_the_same';
        
        }
    }

    public function login(Request $request)
    {

        $email = $request->email;

        $password = $request->password;

        $userObj = User::where('email', $email)->first();

        //登入失敗
        if(!$userObj){
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        //無重設密碼
        if($userObj->is_reset_password==0){

            $credentials = request(['email', 'password']);

            if (! $token = auth()->attempt($credentials)) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }

            return $this->respondWithToken($token);

        //重設密碼
        }else{
            $userResetObj = User::where('email', $email)->where('reset_password', $password)->first();
            
            $reset_password_at = $userResetObj->reset_password_at;

            //可以重設密碼
            if($userResetObj){

                //return 'reset_password_confirm_success';
                return $this->respondWithToken('reset_password_confirm_success');

            //重設密碼錯誤
            }else{

                return response()->json(['error' => 'reset_password_confirm_fail'], 401);
            }

        }
        
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function me()
    {
        return response()->json(auth()->user());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60
        ]);
    }
}