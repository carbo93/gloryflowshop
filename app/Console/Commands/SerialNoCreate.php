<?php

namespace App\Console\Commands;
use App\Models\SerialNo;
use App\Models\SerialProject;
use App\Repositories\SerialNoRepo;
use DB;
use Illuminate\Console\Command;
use File;
use Maatwebsite\Excel\Facades\Excel;

class SerialNoCreate extends Command
{
    // 命令名稱
    protected $signature = 'SerialNo:Create';

    protected $serialNoRepo;
    protected $serialNo;

    // 說明文字
    protected $description = '[序號] 產生序號';

    public function __construct(SerialNoRepo $serialNoRepo, SerialNo $serialNo)
    {
        parent::__construct();
        $this->serialNoRepo = $serialNoRepo;
        $this->serialNo = $serialNo;

    }
    // Console 執行的程式
    public function handle()
    {
        //echo SerianlNo::create();
        $SerialProjectData = SerialProject::where('status','1')->get()->toArray();
        foreach($SerialProjectData as $sdata){
            echo $sdata['id']."_".$sdata['amount'];

            $this->createAmountSerialNo($sdata);
            
        }
        
        // 檔案紀錄在 storage/test.log
        //$log_file_path = storage_path('test2.log');
        //記錄當時的時間
        //$log_info = [
        //    'date'=>date('Y-m-d H:i:s'),
        //];
        // 記錄 JSON 字串
        // $log_info_json = json_encode($log_info) . "\r\n";
        // 記錄 Log
        // File::append($log_file_path, $log_info_json);

        //產生EXCEL
        foreach($SerialProjectData as $sdata){
            $SerialNoData = SerialNo::where('serial_project_id',$sdata['id'])->get()->toArray();
            $file_name = "Glory_SN".$sdata['id']."_".$sdata['amount'];
            
            $excel_data = array();
            $excel_data[0] = ['SN'];
            $i=1;
            foreach($SerialNoData as $sndata){
                $excel_data[$i] = [$sndata["serial_no"]];
                $i++;
            }

            Excel::create($file_name , function($excel) use ($excel_data){

                $excel->sheet('Sheetname', function($sheet) use ($excel_data){

                        $sheet->rows($excel_data);
    
                });
    
            })->store('csv', public_path().'/excel');

/*
            $cellData = [
                ['學號','姓名','成績'],
                ['10001','AAAAA','99'],
                ['10002','BBBBB','92'],
                ['10003','CCCCC','95'],
                ['10004','DDDDD','89'],
                ['10005','EEEEE','96'],
                ];
                Excel::create('學生成績',function ($excel) use ($cellData){
                $excel->sheet('score', function ($sheet) use ($cellData){
                $sheet->rows($cellData);
                });
                })->export('xls');
*/
     
        }

    }

    public function createAmountSerialNo($data){
        $snArray = array();
        for($j=1;$j<=$data['amount'];$j++){
            $sn = $this->serialNoRepo->create();
            $id = 0;

            while($id==0){
                $id = DB::table('serial_no')->insertGetId(
                    [
                        'serial_project_id' => $data['id'], 
                        'creator' => $data['creator'], 
                        'serial_no' => $sn,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s"),
                    ]
                );
            }


            $count = DB::table('serial_no')->where('serial_project_id', $data['id'])->count();
            if($count==$data['amount']){
                $count = DB::table('serial_project')->where('id', $data['id'])->update(['status' => 2]);
            }

        }
    }
}