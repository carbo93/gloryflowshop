<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    $router->resource('project', ProjectController::class);

    $router->resource('banner', BannerController::class);

    $router->resource('contact', ContactController::class);

    $router->resource('info', InfoController::class);

    $router->resource('serial_project', SerialProjectController::class);

    $router->resource('category', CategoryController::class);

    $router->resource('country', CountryController::class);

    $router->resource('product', ProductController::class);

    $router->resource('shipping_fee', ShippingFeeController::class);

    $router->resource('order', OrderController::class);

    $router->resource('logistics', LogisticsController::class);

    $router->resource('payment', PaymentController::class);
   


    //$router->get('serial_project', 'SerialProjectController@index');
    //$router->get('serial_project/create', 'SerialProjectController@create');
    
});

