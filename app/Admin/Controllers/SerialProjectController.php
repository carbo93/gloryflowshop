<?php

namespace App\Admin\Controllers;

use App\Models\SerialNo;
use App\Models\SerialProject;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;

class SerialProjectController extends Controller
{
    use ModelForm;

    public function index(Content $content)
    {
        $content->header('序號管理');
        $content->description('顯示');
        $data = SerialProject::get()->toArray();
        $content->view('admin.serialproject.index', ['data' => $data ]);

        return $content;
    }
    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('序號產生');
            $content->description('編輯');
            $content->body($this->form()->edit($id));
            
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create(Request $request)
    {

        return Admin::content(function (Content $content) {

            $content->header('序號產生');
            $content->description('新增');

            $content->body($this->form());

        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(SerialProject::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->creator()->sortable();
            $grid->name('用途');
            
            $grid->link('下載')->display(function ($link) {

                return "<a href='".$link."'>前往圖片連結</a>";
            
            });;
            $grid->start_time('創建時間')->sortable();
            
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {

        return Admin::form(SerialProject::class, function (Form $form) {

        
            $form->footer(function ($footer) {

                $footer->disableReset();
                //$footer->disableSubmit();
                $footer->disableViewCheck();
                $footer->disableEditingCheck();
                $footer->disableCreatingCheck();
            
            });

            $form->text('creator', '創建者')->value(Admin::user()->name)->icon('fa-ban')->readonly();
            $form->text('name', '用途')->rules('required|min:5', [
                'min'   => '請輸入至少5個字',
            ]);
            /*
            $form->number('amount', '序號數量')->value(1000)->rules('required|regex:/^\d+$/|max:10000', [
                'max'   => '一次最多生產一萬筆序號',
                'regex'   => '請輸入數字',
            ]);
            */
            $data = [];
            for($i=1000;$i<=10000;$i+=1000){
                $data[$i] = $i;
            }

            $form->select('amount', '序號數量')->options($data)->value(1000)->rules('required|regex:/^\d+$/|max:10000', [
                'max'   => '一次最多生產一萬筆序號',
                'regex'   => '請輸入數字',
            ]);

            //$form->display('created_at', '創建時間');
            //$form->display('updated_at', 'Updated At');
        });
    }
}
