<?php

namespace App\Admin\Controllers;
use App\Models\Product;
use App\Models\ProductSpec;
use App\Models\Country;
use App\Models\ShippingFee;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;
use DB;

class ShippingFeeController extends Controller
{
    use ModelForm;
    protected $product;
    protected $country;
    protected $productSpec;

    public function __construct(
        Product $product, 
        Country $country, 
        ProductSpec $productSpec
    )
    {
        $this->product = $product;
        $this->country = $country;
        $this->productSpec = $productSpec;
    }


    public function index(Content $content)
    {
        return Admin::content(function (Content $content) {

            $content->header('運費表');
            $content->description('顯示');
           
            $content->body($this->grid());
        });

    }
    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {

        return Admin::content(function (Content $content) use ($id) {

            $content->header('運費表');
            $content->description('編輯');
            //$content->body($this->form()->edit($id));
            $content->body($this->form($id)->edit($id));
            
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create(Content $content)
    {
        /*
        $countryData = $this->getCountryArray();
        $currencyData = $this->getCurrencyArray();

        $content->header('產品管理');
        $content->description('新增');
        $content->view('admin.product.create',[
            'countryData'=>$countryData,
            'currencyData'=>$currencyData,
            'categoryData'=>$categoryData
        ]);
        return $content;
        */
        
        return Admin::content(function (Content $content) {

            $content->header('運費表');
            $content->description('新增');

            $content->body($this->form());

        });

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(ShippingFee::class, function (Grid $grid) {
            $countryData = $this->getCountryArray();
            $currencyData = $this->getCurrencyArray();
/*
            $grid->tools(function ($tools) {               
                $tools->append("<div class='pull-right'><a href='product/create' class='btn btn-success '><i class='fa fa-plus'></i> 新增單一規格商品</a></div>");
                $tools->append("<div class='pull-right'><a href='product/mutlicreate' class='btn btn-success '><i class='fa fa-plus'></i> 新增多規格商品</a></div>");
            });
*/
            
            $grid->id('ID')->sortable();
            $grid->country_id('國家')->sortable()->display(function($country_id) use ($countryData){
                return $countryData[$country_id]; 
            });
            $grid->shipping_name('運費名稱');
            $grid->min_weight('最小重量(KG)');
            $grid->max_weight('最大重量(KG)');
            $grid->shipping_fee('運費')->display(function($shipping_fee) use ($currencyData){
                return $currencyData[$this->country_id]." ".$shipping_fee;
            });
            
            $grid->actions(function ($actions) {
                $actions->disableView();
                //$actions->disableDelete();
                //$actions->disableEdit();
            });
            //$grid->disableFilter();
            $grid->disableExport();
            $grid->disableRowSelector();
            //$grid->disableActions();
            //$grid->disableCreateButton();
            $grid->disableColumnSelector();

            
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {

        return Admin::form(ShippingFee::class, function (Form $form) use ($id){
            $countryData = $this->getCountryArray();
        
            $form->header(function ($header) {
                $header->disableDelete();
                $header->disableView();
            });
            $form->footer(function ($footer) {

                $footer->disableReset();
                //$footer->disableSubmit();
                $footer->disableViewCheck();
                $footer->disableEditingCheck();
                $footer->disableCreatingCheck();
            
            });


            $form->select('country_id', '國家')->options($countryData);
            $form->text('shipping_name', '運費名稱')->rules('required|max:50', [
                'max'   => '最多50個字',
            ]);
            
            $form->text('min_weight', '最小重量')->rules('required');
            $form->text('max_weight', '最大重量')->rules('required');
            $form->text('shipping_fee', '運費')->rules('required');
                        
        });
    }

    private function getCountryArray(){
        $countryArray = $this->country->get()->toArray();
        $selectData = array();
        foreach($countryArray as $c){
            $selectData[$c['id']] = $c['name'];
        }
        return $selectData;

    }
    private function getCurrencyArray(){
        $countryArray = $this->country->get()->toArray();
        $selectData = array();
        foreach($countryArray as $c){
            $selectData[$c['id']] = $c['currency'];
        }
        return $selectData;

    }

}
