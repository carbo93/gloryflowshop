<?php

namespace App\Admin\Controllers;
use App\Repositories\ProductRepo;
use App\Repositories\CountryRepo;
use App\Models\Product;
use App\Models\ProductSpec;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;
use DB;

class ProductController extends Controller
{
    use ModelForm;
    protected $product;
    protected $productSpec;
    protected $productRepo;
    protected $countryRepo;

    public function __construct(
        Product $product, 
        ProductSpec $productSpec,
        ProductRepo $productRepo,
        CountryRepo $countryRepo
    )
    {
        $this->product = $product;
        $this->productSpec = $productSpec;
        $this->productRepo = $productRepo;
        $this->countryRepo = $countryRepo;
    }


    public function index(Content $content)
    {
        return Admin::content(function (Content $content) {

            $content->header('商品管理');
            $content->description('顯示');
           
            $content->body($this->grid());
        });

    }
    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        //$productData = $this->productRepo->getProductDetailInfo($id);
        //dd($productData);
        return Admin::content(function (Content $content) use ($id) {

            $content->header('商品管理');
            $content->description('編輯');
            //$content->body($this->form()->edit($id));
            $content->body($this->form($id)->edit($id));
            
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create(Content $content)
    {
        /*
        $currencyData = $this->getCurrencyArray();
        $categoryData = $this->getCategoryArray();

        $content->header('產品管理');
        $content->description('新增');
        $content->view('admin.product.create',[
            'countryData'=>$countryData,
            'currencyData'=>$currencyData,
            'categoryData'=>$categoryData
        ]);
        return $content;
        */
        
        return Admin::content(function (Content $content) {

            $content->header('商品管理');
            $content->description('新增');

            $content->body($this->form());

        });

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Product::class, function (Grid $grid) {
            $countryData = $this->countryRepo->getCountryArray();
            $currencyData = $this->countryRepo->getCurrencyArray();
            $categoryData = $this->countryRepo->getCategoryArray();
/*
            $grid->tools(function ($tools) {               
                $tools->append("<div class='pull-right'><a href='product/create' class='btn btn-success '><i class='fa fa-plus'></i> 新增單一規格商品</a></div>");
                $tools->append("<div class='pull-right'><a href='product/mutlicreate' class='btn btn-success '><i class='fa fa-plus'></i> 新增多規格商品</a></div>");
            });
*/
            
            $grid->id('ID')->sortable();
            $grid->country_id('國家')->sortable()->display(function($country_id) use ($countryData){
                return $countryData[$country_id]; 
            });
            $grid->category_id('分類')->sortable()->display(function($country_id) use ($categoryData){
                return $categoryData[$country_id]; 
            });
            $grid->name('商品名稱');
            $grid->type('圖片 / 規格 / 售價')->display(function($status) use ($currencyData){

                $currency = $currencyData[$this->country_id]; 
                /*
                if($status==1){
                    //一種規格
                    //return $this->id;
                    $productSpecObj = ProductSpec::where("product_id",$this->id)->first();
                    //var_dump($productSpecObj);
                    $return_str = $productPicObj->img." / ";
                    $return_str.= $productSpecObj->spec_name." / ";
                    $return_str.= $currency." ".$productSpecObj->price."<br>";
                    
                    return $return_str; 
                }else{
                */
                    //多種規格
                    $productSpecArray = ProductSpec::where("product_id",$this->id)->get();
                    //var_dump($productSpecArray);
                    $return_str = "";
                    foreach($productSpecArray as $p){
                        $return_str.= "<img src='../../uploads/".$p['spec_img']."' width='50px'>  / ";
                        $return_str.= $p['spec_name']." / ";
                        if($p['is_special_price']==1){
                            $return_str.= "<font color='red'>特價 : ".$currency." ".$p['special_price']."</font><br>";
                        }else{
                           $return_str.= "售價  : ".$currency." ".$p['price']."<br>";
                        }
                    }
                    return $return_str; 
                //}
            });
            $grid->status('上／下架')->display(function($status){
                if($status==1){
                    return "<span class='badge alert-success'>上架</span>"; 
                }else{
                    return "<span class='badge alert-danger'>下架</span>"; 
                }
                
            });
            $grid->buy_limit('購買上限');
            $grid->created_at('創建時間');
            $grid->updated_at('更新時間');
            $grid->filter(function ($filter) use ($countryData) {
                $filter->disableIdFilter();
                $filter->equal('country_id', '國家')->select($countryData);
                $filter->like('name', '商品名稱');
                //$filter->between('created_at', '創建時間區間')->datetime();
                
            });
            $grid->actions(function ($actions) {
                $actions->disableView();
                //$actions->disableDelete();
                //$actions->disableEdit();
            });
            //$grid->disableFilter();
            $grid->disableExport();
            $grid->disableRowSelector();
            //$grid->disableActions();
            //$grid->disableCreateButton();
            $grid->disableColumnSelector();

            
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        $theCategory_id=0;
        $theCountry_id=0;
        if ($id) {
            $product = Product::find($id); 
            $theCategory_id = $product->category_id;
            $theCountry_id = $product->country_id;
        }
       
        return Admin::form(Product::class, function (Form $form) use ($id, $theCategory_id, $theCountry_id){

            $countryData = $this->countryRepo->getCountryArray();
            $currencyData = $this->countryRepo->getCurrencyArray();
            $categoryData = $this->countryRepo->getCategoryArray();

        
            $form->footer(function ($footer) {

                $footer->disableReset();
                //$footer->disableSubmit();
                $footer->disableViewCheck();
                $footer->disableEditingCheck();
                $footer->disableCreatingCheck();
            
            });

            
            $form->tab('商品基本資料', function ($form) use ($countryData, $categoryData, $id, $theCategory_id, $theCountry_id){

                

                //修改
                if($id>0){    

                   $form->text('creator', '創建者')->value(Admin::user()->name)->icon('fa-ban')->readonly();

                   $form->text('name', '商品名稱')->rules('required|max:20', [
                       'max'   => '最多20個字',
                   ])->icon('fa-ban')->readonly();
                   
                   $form->text('keyword', '商品短描述')->rules('required|max:10', [
                   
                    'max'   => '最多10個字',
                   ]);

                   $thecountryData[$theCountry_id] = $countryData[$theCountry_id];
                   //$thecategoryData[$theCategory_id] = $categoryData[$theCategory_id];
                   $form->select('country_id', '國家')->options($thecountryData)->readonly();
                   $form->select('category_id', '分類')->options($categoryData)->value($theCategory_id);

                   $form->number('buy_limit', '購買上限')->value(9)->readonly();
               
                   //新增   
                }else{
                   $form->text('creator', '創建者')->value(Admin::user()->name)->icon('fa-ban')->readonly();
                   
                   $form->text('name', '商品名稱')->rules('required|max:15', [
                       'max'   => '最多15個字',
                   ]);
                   $form->text('keyword', '商品短描述')->rules('required|max:10', [
                    'max'   => '最多10個字',
                   ]);
                   $form->select('country_id', '國家')->options($countryData)->value(1)->rules('required')->load('category_id', '/api/country/category?theCategory_id='.$theCategory_id."&theCountry_id=".$theCountry_id);
                   
                   $form->select('category_id', '分類')->options($this->countryRepo->getCategoryArray($theCountry_id))->rules('required');    
                   
                   $form->number('buy_limit', '購買上限')->value(9)->rules('required|regex:/^\d+$/|max:100', [
                    'max'   => '一人最多只能買一百個',
                    'regex'   => '請輸入數字',
                   ]);

                }

                $states = [
                    'on'  => ['value' => 1, 'text' => '上架', 'color' => 'success'],
                    'off' => ['value' => 2, 'text' => '下架', 'color' => 'danger'],
                ];
                
                $form->switch('status', '上/下架')->states($states);

            })->tab('商品圖片', function ($form) {

                $form->image('img1', '商品圖片1')->rules('required');
                $form->image('img2', '商品圖片2');
                $form->image('img3', '商品圖片3');

            })->tab('商品描述', function ($form) {

                $form->editor('desc', '商品描述內容')->rules('required');
            
            })->tab('商品規格', function ($form) {
                $form->hasMany('productspecs', '', function (Form\NestedForm $form) {
                        //$form->image('spec_img', '商品規格圖')->addElementClass(uniqid())->thumbnail('small', $width = 100, $height = 100);;                   

                        $form->image('spec_img', '商品規格圖')->addElementClass(uniqid());
                        $form->text('spec_name','商品規格名稱')->rules('required');
                        $form->text('weight', '商品重量(KG)')->rules('required');
                        $form->text('original_price','商品原價')->rules('required');
                        $form->text('price','商品售價')->rules('required');
                        $form->text('special_price','商品特價')->rules('required');
                        $states = [
                            'off' => ['value' => 0, 'text' => '否', 'color' => 'danger'],
                            'on'  => ['value' => 1, 'text' => '是', 'color' => 'success'],
                            
                        ];
                        $form->switch('is_special_price', '是否要開啟特價')->states($states);
                        $form->text('amount','商品可販賣數量')->rules('required');

                });
            
            });


            
        });
    }

   
}
