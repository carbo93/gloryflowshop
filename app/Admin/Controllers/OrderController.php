<?php

namespace App\Admin\Controllers;
use App\Models\Order;
use App\Models\OrderList;
use App\Models\GloryUserPaymentInfo;
use App\Repositories\CountryRepo;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;
use Encore\Admin\Widgets\Table;
use DB;

class OrderController extends Controller
{
    use ModelForm;
    protected $order;
    protected $orderList;
    protected $countryRepo;
    protected $gloryUserPaymentInfo;

    public function __construct(
        Order $order, 
        OrderList $orderList,
        CountryRepo $countryRepo,
        GloryUserPaymentInfo $gloryUserPaymentInfo
    )
    {
        $this->order = $order;
        $this->orderList = $orderList;
        $this->countryRepo = $countryRepo;
        $this->gloryUserPaymentInfo = $gloryUserPaymentInfo;
    }


    public function index(Content $content)
    {
        return Admin::content(function (Content $content) {

            $content->header('訂單查詢');
            $content->description('顯示');
           
            $content->body($this->grid());
        });

    }
    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('訂單查詢');
            $content->description('編輯');
            $content->body($this->form($id)->edit($id));
            
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create(Content $content)
    {
        
        return Admin::content(function (Content $content) {

            $content->header('訂單查詢');
            $content->description('新增');

            $content->body($this->form());

        });

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Order::class, function (Grid $grid) {
            $countryData = $this->countryRepo->getCountryArray();
            $currencyData = $this->countryRepo->getCurrencyArray();

/*
            $grid->tools(function ($tools) {               
                $tools->append("<div class='pull-right'><a href='product/create' class='btn btn-success '><i class='fa fa-plus'></i> 新增單一規格商品</a></div>");
                $tools->append("<div class='pull-right'><a href='product/mutlicreate' class='btn btn-success '><i class='fa fa-plus'></i> 新增多規格商品</a></div>");
            });
*/
            
            //$grid->id('ID')->sortable();
            //$grid->order_id('訂單編號')->sortable();

            $grid->column('order_id', '訂單編號')->expand(function ($model) {

                $order_id= $model->order_id;

                $orderlists = OrderList::select('product_name', 'spec_name', 'weight', 'is_special_price', 'price', 'special_price', 'num', 'total_price', 'total_special_price')->where('order_id', $order_id)->get();
                /*
                $orderlists = $model->orderlists()->take(10)->map(function ($orderlist) {
                    return $orderlist->only(['id', 'spec_name', 'created_at']);
                });
                */

                $returnData = [];
                $orderData = $orderlists->toArray();
                //dd($orderData);
                $i=0;
                foreach($orderData as $o){
                    
                    $returnData[$i]['product_name'] = $o['product_name']."(".$o['spec_name'].")";
                    
                    $returnData[$i]['weight'] = $o['weight']."kg";
                    
                    if($o["is_special_price"]==1){
                    
                        $returnData[$i]['price'] = "<span style='font-color:red'>NT$".$o['special_price']."</span><span style='text-decoration:line-through;'>NT$".$o['price']."</span>";
                    
                    }else{
                    
                        $returnData[$i]['price'] = "NT$".$o['price'];
                    
                    }
        
                    $returnData[$i]['num'] = $o['num'];
                    
                    $total_special_price = $o['special_price']*$o['num'];
                    
                    $total_price = $o['price']*$o['num'];
                    
                    if($o["is_special_price"]==1){
                        
                        $returnData[$i]['total_price'] = "NT$".$total_special_price."<span style='text-decoration:line-through;'>NT$".$total_price."</span>";

                    }else{

                        $returnData[$i]['total_price'] = "NT$".$total_price ;
                    
                    }

                    $i++;

                }

                return new Table([ '商品名稱(規格)', '重量', '商品單一售價', '銷售數量', '商品總售價' ], $returnData);
                
            });


            $grid->user_id('使用者ID')->sortable();
            $grid->user_email('使用者E-mail')->sortable();
            $grid->country_id('國家')->sortable()->display(function($country_id) use ($countryData){
                return $countryData[$country_id]; 
            });
            

            $grid->total_price('售價總額')->display(function($total_price) use ($currencyData){
                return $currencyData[$this->country_id]." ".$total_price; 
            });
            
            $grid->shipping_fee('運費')->display(function($shipping_fee) use ($currencyData){
                return $currencyData[$this->country_id]." ".$shipping_fee; 
            });

            $grid->user_pay('使用者實際付費')->display(function($user_pay) use ($currencyData){
                return "<span class='badge alert-info'>".$currencyData[$this->country_id]." ".$user_pay."</span>"; 
            });


            $grid->receiver_name('收貨人名稱');
            $grid->phone('收貨人電話');
            $grid->zipcode('郵遞區號');
            $grid->address('收貨人地址');
            $grid->receiver_note('收貨人備註');

            //$grid->user_payment_type('付費方式');


            $grid->column('user_payment_type', '付費方式')->expand(function ($model) {

                switch($model->user_payment_type){
                    case 1:
                        return new Table(['信用卡資訊'], [[$model->user_payment_info]]);
                    break;
                }
                
                
            });



            //$grid->user_payment_info('付費資訊');

           
            $grid->order_shipping_status('訂單狀態')->using(
                [
                    '0' => "<span class='badge'>訂單建立</span>", 
                    '1' => "<span class='badge'>訂單確認</span>", 
                    '2' => "<span class='badge'>已出貨</span>", 
                    '3' => "<span class='badge'>配送中</span>", 
                    '4' => "<span class='badge alert-success'>已簽收</span>",
                    '5' => "<span class='badge alert-warning'>退貨/退款</span>",

                ]
            );

            $grid->order_payment_status('交易狀態')->using(
                [
                    '0' => "<span class='badge'>尚未付款</span>", 
                    '1' => "<span class='badge alert-success'>已付款/授權成功</span>", 
                    '2' => "<span class='badge'>未處理</span>", 
                    '3' => "<span class='badge'>進入交易頁面</span>", 
                    '4' => "<span class='badge alert-info'>銀行交易中</span>",
                    '5' => "<span class='badge alert-warning'>使用者取消</span>",
                    '6' => "<span class='badge alert-warning'>交易失敗</span>"
                ]
            );
            

            $grid->created_at('創建時間');
            //$grid->updated_at('更新時間');
           
            $grid->filter(function ($filter) use ($countryData) {
                $filter->disableIdFilter();
                $filter->equal('country_id', '國家')->select($countryData);
                $filter->equal('order_id', '訂單編號');
                $filter->equal('user_email', '使用者E-mail');
                //$filter->between('created_at', '創建時間區間')->datetime();
                
            });

            $grid->actions(function ($actions) {
                $actions->disableView();
                //$actions->disableDelete();
                //$actions->disableEdit();
            });
            //$grid->disableFilter();
            $grid->disableExport();
            $grid->disableRowSelector();
            $grid->disableActions();
            $grid->disableCreateButton();
            $grid->disableColumnSelector();

            
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    /*
    protected function form($id = null)
    {


        return Admin::form(Order::class, function (Form $form) use ($id){
            $countryData = $this->getCountryArray();
            $categoryData = $this->getCategoryArray();
        
            $form->footer(function ($footer) {

                $footer->disableReset();
                //$footer->disableSubmit();
                $footer->disableViewCheck();
                $footer->disableEditingCheck();
                $footer->disableCreatingCheck();
            
            });

            
            $form->tab('商品基本資料', function ($form) use ($countryData, $categoryData, $id, $theCategory_id, $theCountry_id){

                

                //修改
                if($id>0){    

                   $form->text('creator', '創建者')->value(Admin::user()->name)->icon('fa-ban')->readonly();

                   $form->text('name', '商品名稱')->rules('required|max:20', [
                       'max'   => '最多20個字',
                   ])->icon('fa-ban')->readonly();

                   $thecountryData[$theCountry_id] = $countryData[$theCountry_id];
                   //$thecategoryData[$theCategory_id] = $categoryData[$theCategory_id];
                   $form->select('country_id', '國家')->options($thecountryData)->readonly();
                   $form->select('category_id', '分類')->options($categoryData)->value($theCategory_id);

                   $form->number('buy_limit', '購買上限')->value(9)->readonly();
               
                   //新增   
                }else{
                   $form->text('creator', '創建者')->value(Admin::user()->name)->icon('fa-ban')->readonly();
                   
                   $form->text('name', '商品名稱')->rules('required|max:20', [
                       'max'   => '最多20個字',
                   ]);
                   $form->select('country_id', '國家')->options($countryData)->value(1)->rules('required')->load('category_id', '/api/country/category?theCategory_id='.$theCategory_id."&theCountry_id=".$theCountry_id);
                   
                   $form->select('category_id', '分類')->options($this->getCategoryArray($theCountry_id))->rules('required');    
                   
                   $form->number('buy_limit', '購買上限')->value(9)->rules('required|regex:/^\d+$/|max:100', [
                    'max'   => '一人最多只能買一百個',
                    'regex'   => '請輸入數字',
                   ]);

                }

                $states = [
                    '上架'  => ['value' => 1, 'text' => '上架', 'color' => 'success'],
                    '下架' => ['value' => 2, 'text' => '下架', 'color' => 'danger'],
                ];
                
                $form->switch('status', '上/下架')->states($states);

            })->tab('商品圖片', function ($form) {

                $form->image('img1', '商品圖片1')->rules('required');
                $form->image('img2', '商品圖片2');
                $form->image('img3', '商品圖片3');

            })->tab('商品描述', function ($form) {

                $form->editor('desc', '商品描述內容')->rules('required');
            
            })->tab('商品規格', function ($form) {
                $form->hasMany('productspecs', '', function (Form\NestedForm $form) {
                        //$form->image('spec_img', '商品規格圖')->addElementClass(uniqid())->thumbnail('small', $width = 100, $height = 100);;                   

                        $form->image('spec_img', '商品規格圖')->addElementClass(uniqid());

                        $form->text('spec_name','商品規格名稱')->rules('required');
                        
                        $form->text('weight', '商品重量(KG)')->rules('required');

                        $form->text('original_price','商品原價')->rules('required|regex:/^\d+$/', [
                            'regex'   => '請輸入數字',
                        ]);
   
                        $form->text('price','商品售價')->rules('required|regex:/^\d+$/', [
                            'regex'   => '請輸入數字',
                        ]);
    
                        $form->text('special_price','商品特價')->rules('required|regex:/^\d+$/', [
                            'regex'   => '請輸入數字',
                        ]);
 
                        $form->text('amount','商品可販賣數量')->rules('required|regex:/^\d+$/', [
                            'regex'   => '請輸入數字',
                        ]);

                });
            
            });


            
        });
    }
    */

 
}
