<?php

namespace App\Admin\Controllers;

use App\Models\Contact;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class ContactController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('聯絡我們');
            $content->description('列表');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('聯絡我們');
            $content->description('編輯');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('聯絡我們');
            $content->description('新增');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Contact::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->name('姓名');
            $grid->phone('電話');
            $grid->available_time('聯絡時間')->display(function ($available_time) {
                switch($available_time){
                    case 1:
                        return '10點~12點';
                        break;
                    case 2:
                        return '12點~14點';
                        break;
                    case 3:
                        return '14點~16點';
                        break;
                    case 4:
                        return '16點~18點';
                        break;
                    case 5:
                        return '18點以後';
                        break;
                }
            });
            $grid->content('留言')->display(function ($content) {

                return mb_substr($content,0,20,"utf-8")."...";
            
            });
            $grid->created_at('留言時間')->sortable();
            $grid->disableCreation();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Contact::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->display('name','姓名');
            $form->display('phone','電話');
            $form->display('available_time','聯絡時間');
            $form->display('content','留言');
            $form->display('created_at','留言時間');
        });
    }
}
