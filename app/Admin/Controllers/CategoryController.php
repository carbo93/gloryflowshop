<?php

namespace App\Admin\Controllers;
use App\Models\Category;
use App\Repositories\CountryRepo;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use ModelForm;
    protected $countryRepo;

    public function __construct(
        CountryRepo $countryRepo
    )
    {
        $this->countryRepo = $countryRepo;
    }

    public function index(Content $content)
    {
        return Admin::content(function (Content $content) {

            $content->header('商品分類維護');
            $content->description('列表');

            $content->body($this->grid());
        });
    }
    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('商品分類維護');
            $content->description('編輯');
            $content->body($this->form()->edit($id));
            
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create(Request $request)
    {

        return Admin::content(function (Content $content) {

            $content->header('商品分類維護');
            $content->description('新增');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $selectData = $this->countryRepo->getCountryArray();
        return Admin::grid(Category::class, function (Grid $grid) use ($selectData) {
            //if($this->country_id==1||$this->country_id==''){ 
            //$grid->model()->where('country_id', '=', 1);
            //}
            $grid->id('ID')->sortable();;
            $grid->country_id('國家')->display(function ($country_id) use ($selectData) {
                return $selectData[$country_id];
            })->sortable();;
            $grid->name('分類名稱');
            //$grid->product_amount('商品數量');
            $grid->index_show('首頁顯示')->display(function ($index_show) {
                if($index_show==1){
                    return '<span class="badge alert-success">顯示</span>';
                }else{
                    return '<span class="badge alert-danger">不顯示</span>';
                }
            })->sortable();;
            //$grid->disableCreation();
            //$grid->disablePagination();
            //$grid->disableFilter();
            $grid->disableExport();
            $grid->disableRowSelector();
            //$grid->disableActions();
            $grid->disableColumnSelector();
            $grid->actions(function ($actions) {
                $actions->disableView();
                //$actions->disableDelete();
                //$actions->disableEdit();
            });
            $grid->filter(function($filter) use ($selectData){
                $filter->disableIdFilter();
                $filter->equal('country_id', '請選擇國家')->select($selectData);
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $selectData = $this->countryRepo->getCountryArray();
        return Admin::form(Category::class, function (Form $form) use ($selectData) {

            $form->header(function ($header) {
                $header->disableDelete();
                $header->disableView();
            });
            $form->footer(function ($footer) {

                $footer->disableReset();
                //$footer->disableSubmit();
                $footer->disableViewCheck();
                $footer->disableEditingCheck();
                $footer->disableCreatingCheck();
            
            });

            $form->select('country_id', '國家')->options($selectData)->rules('required');
            $form->text('name', '分類名稱');
            $form->switch('index_show', '首頁顯示');
        });
    }

}
