<?php

namespace App\Admin\Controllers;

use App\Models\Logistics;
use App\Repositories\CountryRepo;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class LogisticsController extends Controller
{
    use ModelForm;
    protected $countryRepo;
    protected $logistics;

    public function __construct(
        CountryRepo $countryRepo,
        Logistics $logistics
    )
    {
        $this->countryRepo = $countryRepo;
        $this->logistics = $logistics;
    }
    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('物流維護');
            $content->description('列表');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('物流維護');
            $content->description('編輯');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('物流維護');
            $content->description('新增');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $selectData = $this->countryRepo->getCountryArray();
        return Admin::grid(Logistics::class, function (Grid $grid) use ($selectData){

            $grid->id('ID')->sortable();
            $grid->country_id('國家')->display(function ($country_id) use ($selectData) {
                return $selectData[$country_id];
            })->sortable();
            $grid->name('物流名稱');
            $grid->logistic_id('物流商提供的ID')->display(function ($logistic_id) {
                if($logistic_id==''){
                   return "無";
                }else{
                   return $logistic_id;
                }
            });

            $grid->status('是否顯示')->display(function($status){
                if($status==1){
                    return "<span class='badge alert-success'>是</span>"; 
                }else{
                    return "<span class='badge alert-danger'>否</span>"; 
                }
                return 'check with ';
                
            });
            $grid->actions(function ($actions) {
                $actions->disableView();
                //$actions->disableDelete();
                //$actions->disableEdit();
            });
            //$grid->disableFilter();
            $grid->disableExport();
            $grid->disableRowSelector();
            //$grid->disableActions();
            //$grid->disableCreateButton();
            $grid->disableColumnSelector();
            $grid->filter(function($filter) use ($selectData){
                $filter->disableIdFilter();
                $filter->equal('country_id', '請選擇國家')->select($selectData);
            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $selectData = $this->countryRepo->getCountryArray();
        return Admin::form(Logistics::class, function (Form $form) use ($selectData)  {

            $form->header(function ($header) {
                $header->disableDelete();
                $header->disableView();
            });
            $form->footer(function ($footer) {

                $footer->disableReset();
                //$footer->disableSubmit();
                $footer->disableViewCheck();
                $footer->disableEditingCheck();
                $footer->disableCreatingCheck();
            
            });
            //$form->display('id', 'ID');
            $form->select('country_id', '國家')->options($selectData)->rules('required');
            $form->text('name', '物流名稱');
            $form->text('logistic_id', '物流商提供的ID (非必填)');

            $states = [
                'off' => ['value' => 0, 'text' => '否', 'color' => 'danger'],
                'on'  => ['value' => 1, 'text' => '是', 'color' => 'success'], 
            ];
            $form->switch('status', '是否顯示')->states($states);

        });
    }
}
