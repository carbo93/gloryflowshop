<?php

namespace App\Admin\Controllers;

use App\Models\Banner;
use App\Repositories\CountryRepo;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class BannerController extends Controller
{
    use ModelForm;
    protected $countryRepo;

    public function __construct(
        CountryRepo $countryRepo
    )
    {
        $this->countryRepo = $countryRepo;
    }
    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('廣告');
            $content->description('列表');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('廣告');
            $content->description('編輯');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('廣告');
            $content->description('新增');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $selectData = $this->countryRepo->getCountryArray();
        return Admin::grid(Banner::class, function (Grid $grid) use ($selectData){

            $grid->id('ID')->sortable();
            $grid->country_id('國家')->display(function ($country_id) use ($selectData) {
                return $selectData[$country_id];
            })->sortable();
            $grid->img_name('圖片')->image('',100, 100);
            $grid->link('連結')->display(function ($link) {

                return "<a href='".$link."'>前往圖片連結</a>";
            
            });;
            $grid->start_time('開始時間');
            $grid->end_time('結束時間');

            $grid->actions(function ($actions) {
                $actions->disableView();
                //$actions->disableDelete();
                //$actions->disableEdit();
            });
            //$grid->disableFilter();
            $grid->disableExport();
            $grid->disableRowSelector();
            //$grid->disableActions();
            //$grid->disableCreateButton();
            $grid->disableColumnSelector();
            $grid->filter(function($filter) use ($selectData){
                $filter->disableIdFilter();
                $filter->equal('country_id', '請選擇國家')->select($selectData);
            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $selectData = $this->countryRepo->getCountryArray();
        return Admin::form(Banner::class, function (Form $form) use ($selectData)  {

            $form->header(function ($header) {
                $header->disableDelete();
                $header->disableView();
            });
            $form->footer(function ($footer) {

                $footer->disableReset();
                //$footer->disableSubmit();
                $footer->disableViewCheck();
                $footer->disableEditingCheck();
                $footer->disableCreatingCheck();
            
            });
            //$form->display('id', 'ID');
            $form->select('country_id', '國家')->options($selectData)->rules('required');
            $form->text('link', '連結');
            //$form->file('file_column');
            $form->image('img_name', '圖片');
            $form->datetime('start_time', '開始時間');
            $form->datetime('end_time', '結束時間');
            //$form->display('created_at', 'Created At');
            //$form->display('updated_at', 'Updated At');
        });
    }
}
