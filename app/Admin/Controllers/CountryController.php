<?php

namespace App\Admin\Controllers;

use App\Models\Country;
use App\Models\Category;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    use ModelForm;

    public function index(Content $content)
    {
        return Admin::content(function (Content $content) {

            $content->header('國家維護');
            $content->description('列表');

            $content->body($this->grid());
        });
    }
    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('國家維護');
            $content->description('編輯');
            $content->body($this->form()->edit($id));
            
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create(Request $request)
    {

        return Admin::content(function (Content $content) {

            $content->header('國家維護');
            $content->description('新增');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Country::class, function (Grid $grid) {

            $grid->id('ID');
            $grid->name('國家名稱');
            $grid->code('國家代號');
            $grid->language_name('國家語言名稱');
            $grid->language_code('國家語系代號');
            $grid->currency('貨幣');
            $grid->area_code('國際冠碼');
            $grid->disableFilter();
            $grid->disableExport();
            $grid->disableRowSelector();
            //$grid->disableActions();
            $grid->disableColumnSelector();
            $grid->actions(function ($actions) {
                $actions->disableView();
                $actions->disableDelete();
                //$actions->disableEdit();
            });
            
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {

        return Admin::form(Country::class, function (Form $form) {


            $form->header(function ($header) {
                $header->disableDelete();
                $header->disableView();
            });
            $form->footer(function ($footer) {

                $footer->disableReset();
                //$footer->disableSubmit();
                $footer->disableViewCheck();
                $footer->disableEditingCheck();
                $footer->disableCreatingCheck();
            
            });

            $form->text('name', '國家名稱');
            $form->text('code', '國家代號');
            $form->text('language_name','國家語言名稱');
            $form->text('language_code','國家語系代號');
            $form->text('currency','貨幣');
            $form->text('area_code','國際冠碼');
        });
    }

}
