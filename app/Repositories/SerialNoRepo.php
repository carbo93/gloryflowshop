<?php 
namespace App\Repositories;

class SerialNoRepo
{


    public function create()
    {
        $length = 16;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {

            $randomString .= $characters[rand(0, $charactersLength - 1)];
            if($i==3||$i==7||$i==11){
                $randomString .= "-";
            }
        }
        return $randomString;

    }
} 