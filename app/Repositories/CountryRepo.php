<?php 
namespace App\Repositories;
use App\Models\Country;
use App\Models\Category;

class CountryRepo
{
   
    protected $country;
    protected $category;
    
    public function __construct(
        Country $country,
        Category $category
    )
    {
        $this->country = $country;
        $this->category = $category;
    }

    public function getCountryArray(){
        $countryArray = $this->country->get()->toArray();
        $selectData = array();
        foreach($countryArray as $c){
            $selectData[$c['id']] = $c['name'];
        }
        return $selectData;

    }
    public function getCurrencyArray(){
        $countryArray = $this->country->get()->toArray();
        $selectData = array();
        foreach($countryArray as $c){
            $selectData[$c['id']] = $c['currency'];
        }
        return $selectData;

    }
    
    public function getCategoryArray($country_id = null){
        if($country_id==null){ 
           $categoryArray = $this->category->get()->toArray();
        }else{
           $categoryArray = $this->category->where('country_id', $country_id)->get()->toArray();     
        }
        $categoryData = array();
        foreach($categoryArray as $c){
            $categoryData[$c['id']] = $c['name'];
        }
        return $categoryData;

    }
} 