<?php 
namespace App\Repositories;
use App\Models\Product;
use App\Models\ProductSpec;
//use App\Models\Country;
//use App\Models\Category;
use DB;
class ProductRepo
{
   
    //protected $country;
    //protected $category;
    protected $product;
    protected $productSpec;
    

    public function __construct(
        //Country $country, 
        //Category $category, 
        Product $product,  
        ProductSpec $productSpec
    )
    {
        //$this->country = $country;
        //$this->category = $category;
        $this->product = $product;
        $this->productSpec = $productSpec;
    }

   static public function getProductDetailInfo($product_id)
    {
        $img_url = env('IMG_URL');

        $product = Product::where('id', $product_id)
        ->get(
            [
                'id', 
                'name', 
                'desc', 
                'keyword',
                'category_id', 
                'country_id', 
                'buy_limit', 
                'status',
                'img1',
                'img2',
                'img3', 
                'special_price_start_time', 
                'special_price_end_time'
            ]
        );


        if(count($product)>0){

          $product[0]->product_spec = ProductSpec::where('product_id', $product_id)
          ->get([
              DB::raw('id as product_spec_id'),
              'spec_name',
              'weight',
              'price',
              'special_price', 
              'is_special_price',
              'spec_img'
            ]);

        }

        if($product[0]->img1!=''){
           $product[0]->img1 = $img_url.$product[0]->img1;
        }
        if($product[0]->img2!=''){
           $product[0]->img2 = $img_url.$product[0]->img2;
        }
        if($product[0]->img3!=''){
           $product[0]->img3 = $img_url.$product[0]->img3;
        }
        foreach($product[0]->product_spec as $spec){
           if($spec->spec_img!=''){
              $spec->spec_img = $img_url.$spec->spec_img;
           }
        }

        return $product;

    }
} 