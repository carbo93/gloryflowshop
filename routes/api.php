<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

    /********* １. Category  *********/
    //1.1 商品分類API
    //Route::middleware('cors')->get('country/category', 'CountryController@category');
    Route::get('country/category', 'CountryController@category');
    //1.2 取得分類內的商品
    Route::get('category/product_list', 'CategoryController@product_list');
    //1.3 取得首頁可顯示的分類
    Route::get('category/index_show', 'CategoryController@index_show');

    /********* 2. Banner *********/
    //2.1 Banner API
    Route::get('banner/list', 'BannerController@list');

    /********* 3. Product *********/
    //3.1 取得單一個Product詳細資訊 API
    Route::get('product', 'ProductController@getOneProductInfo');
    //3.2 紀錄商品被瀏覽次數
    Route::get('product/click', 'ProductController@product_click');
    //3.3 商品搜尋
    Route::get('product/search', 'ProductController@search');
    //3.4 最新商品
    Route::get('product/new', 'ProductController@new');

    /********* 4. Order *********/
    //4.1 取得使用者訂單
    Route::post('order/list', 'OrderController@getUserOrderList');
    //4.2 建立訂單
    Route::post('order/create', 'OrderController@createAnOrder');


    /********* 5. Favorite *********/
    //5.1 加到我的最愛
    Route::post('favorite/add', 'FavoriteController@add');
    //5.2 刪除我的最愛
    Route::post('favorite/delete', 'FavoriteController@delete');
    //5.3 取得我的最愛
    Route::post('favorite/list', 'FavoriteController@list');

    /********* 6. ShoppingCart *********/
    //6.1 加到我的購物車
    Route::get('shoppingCart/add', 'ShoppingCartController@add');
    //6.2 刪除我的最愛
    Route::get('shoppingCart/delete', 'ShoppingCartController@delete');
    //6.3 更新我的最愛
    Route::get('shoppingCart/update', 'ShoppingCartController@update');


    /********* 7. 使用者登入註冊 *********/
    Route::get('/me', 'AuthController@me')->name('me');
    Route::post('/register', 'AuthController@register')->name('register');
    Route::post('/login', 'AuthController@login')->name('login');
    Route::get('/logout', 'AuthController@logout')->name('logout');
    Route::post('/reset_password', 'AuthController@reset_password')->name('reset_password');



    /********* 8. 寄信相關 *********/
    Route::post('/email/forget_password', 'EmailController@forgetPassword')->name('forgetPassword');


    /********* 9. 物流相關 *********/
    Route::get('/logistics/list', 'LogisticsController@list')->name('logistics_list');


    /********* 10. 金流相關 *********/
    Route::get('/payment/list', 'PaymentController@list')->name('payment_list');


    /********* 11. 國家相關 *********/
    Route::get('/country/list', 'CountryController@list')->name('country_list');
